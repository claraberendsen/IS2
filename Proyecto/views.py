from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from datetime import datetime
from guardian.decorators import permission_required_or_403
from django.views import generic
from django.shortcuts import render
from django.contrib.auth.mixins import PermissionRequiredMixin
# Login view
from django.views.generic.edit import CreateView, UpdateView

# Create your views here.
from weasyprint import HTML

from Kanban.models import Flujo
from Proyecto.models import Proyecto
from Proyecto.forms import  ProyectoForm, ProyectoModForm

from django.views.generic.edit import CreateView, UpdateView

# Create your views here.
from Accounts.models import User,Rol
from Proyecto.models import Proyecto
from Sprint.models import  Sprint
from UserStory.models import UserStory
from team.models import team
from Proyecto.forms import ProyectoForm



class Crear_Proyecto(PermissionRequiredMixin, CreateView):
    """
    Crear_Proyecto hereda de de la vista generica CreateView y PermissionRequiredMixin
    Requiere que el cliente possee el  permiso crear_proyecto para poder ser utilizada
    """
    model = Proyecto
    template_name = 'pro/pro_crear.html'
    form_class = ProyectoForm
    success_url = reverse_lazy('Proyecto:listaProyectos')

    permission_required = 'Proyecto.crear_proyecto'
    raise_exception = True

    def form_valid(self, form):
        self.object = form.save()
        user = User.objects.get(pk=form.data['user'])
        rol = Rol.objects.get(pk=form.data['rol'])
        team.objects.create(user=user, rol=rol, horas_trabajadas=form.data['horas_trabajadas'], proyecto=self.object)
        return  HttpResponseRedirect(self.get_success_url())

class Modificar_Proyecto(PermissionRequiredMixin, UpdateView):
    """
       Modificar_Proyecto hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el cliente possee el  permiso modificar_proyecto para poder ser utilizada
    """
    model = Proyecto
    template_name = 'pro/pro_mod.html'
    form_class = ProyectoModForm
    permission_required = 'Proyecto.modificar_proyecto'
    success_url = reverse_lazy('Proyecto:listaProyectos')
    raise_exception = True

    def post(self, request, *args, **kwargs):
        """
        Se define la funcion post para detectar cuando se intenta eliminar un elemento y  redireccionar el pedido a
        la url Proyecto;Team:eliminarTeam
        :param request:
        :param args:
        :param kwargs: Parametros necesarios para la view EliminarTeam
        :return: La funcion post de la clase parent
        """
        if 'delete' in request.POST:
            return redirect('Proyecto:eliminarProyecto', **kwargs)
        return super().post(request, *args, **kwargs)


    # def form_valid(self, form):
    #     # This method is called when valid form data has been POSTed.
    #     # It should return an HttpResponse.
    #     return super().form_valid(form)
    # def get(self, request, *args, **kwargs):
    #     request.session['proy'] = kwargs.get('pk')
    #     return super().get(self, request, *args, **kwargs)


class HomeProyectos(PermissionRequiredMixin, generic.ListView):
    """

     """
    model = Proyecto
    template_name = 'pro/pro_home.html'
    context_object_name = 'my_proyectos'
    permission_required = []
    raise_exception = True

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)


    def get_context_data(self, *, object_list=None, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        return context


class ListaProyectos(PermissionRequiredMixin, generic.ListView):
    """
      Listar_Proyecto hereda de de la vista generica ListView y PermissionRequiredMixin
      Requiere que el cliente possee el  permiso Listar_proyecto para poder ser utilizada
     """
    model = Proyecto
    template_name = 'pro/pro_listar.html'
    context_object_name = 'my_proyectos'
    permission_required = []
    raise_exception = True



    def get_queryset(self):
        user = self.request.user
        if user.has_perm('Proyecto.ver_todos_proyectos'):
            return Proyecto.objects.all()
        teams = user.team_set.all()
        list = []
        for team in teams:
            list.append(team.proyecto)
        return list

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ListaProyectos, self).get_context_data()

        user = self.request.user
        teams = user.team_set.all()
        list = []
        for team in teams:
            list.append(team.proyecto)
        context['lista'] = list
        return context



class EliminarProyecto(PermissionRequiredMixin, generic.DeleteView):
    model = Proyecto
    template_name = 'pro/pro_listar.html'
    permission_required = 'Proyecto.eliminar_proyecto'
    raise_exception = True
    success_url = reverse_lazy('Proyecto:listaProyectos')
    raise_exception = True

@permission_required_or_403('Proyecto.eliminar_proyecto')
def CancelarProyecto(request, pk):
    """
    Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
    :param kwargs: pk
    :return: El nuevo context
    """

    proyecto = Proyecto.objects.get(pk=pk)
    flag = True
    qs = Sprint.objects.filter(proyecto=pk).exclude(estado=1)
    for sprint in qs:
        if sprint.estado != 3:
            flag = False
            sprint.estado = 4
            sprint.save()

    qs = UserStory.objects.filter(proyecto=pk).exclude(estado=1)

    for us in qs:
        if us.estado != 3:
            flag=False
            us.estado = 4
            us.save()
    if flag:
        proyecto.estado = 3
    else:
        proyecto.estado = 4

    proyecto.fechafin = datetime.now().date()
    proyecto.save()

    return HttpResponseRedirect(reverse_lazy('Proyecto:listaProyectos'))

@permission_required_or_403('Proyecto.iniciar_proyecto')
def iniciarProyectoView(request, pk):
    """
    Inicia el sprint seleccionado, setea las fechas iniciales y fechas finales estimadas, el estado y los estados y fases
    de los user stories del sprint backlog
    :param request: el request http
    :param pk: el primary key del proyecto
    :param pks: el primary key del sprint
    :return: retorna una redireccion htttp a la lista de sprints
    """

    proyecto = Proyecto.objects.get(pk=pk)
    proyecto.fechainicio = datetime.now().date()

    proyecto.estado = 2
    proyecto.save()

    return HttpResponseRedirect(reverse_lazy('Proyecto:listaProyectos'))


class VerificarTerminarProyecto(PermissionRequiredMixin, generic.UpdateView):
    model = Proyecto
    fields = ['estado']
    template_name = 'pro/proyecto_confirm_delete.html'
    permission_required = 'Proyecto.eliminar_proyecto'
    raise_exception = True

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        flag = True
        qs = Sprint.objects.filter(proyecto=self.kwargs['pk']).exclude(estado=1)
        for sprint in qs:
            if sprint.estado != 3:
                flag = False
                sprint.estado = 4
                sprint.save()

        qs = UserStory.objects.filter(proyecto=self.kwargs['pk']).exclude(estado=1)

        for us in qs:
            if us.estado != 3:
                flag=False
                us.estado = 4
                us.save()
        context['flag_terminado'] = flag
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:terminarProyectos')



def reporte_product_backlog(request, pk):
    proyecto = get_object_or_404(Proyecto, pk=pk)

    context = {
        'user_stories_inicial' : proyecto.userstory_set.filter(estado=1),
        'user_stories_en_desarrollo' : proyecto.userstory_set.filter(estado=2),
        'user_stories_terminados' : proyecto.userstory_set.filter(estado=3),
        'user_stories_cancelados' : proyecto.userstory_set.filter(estado=4),
        'proyecto': proyecto
    }

    html_string = render_to_string('reportes/pdf_template_product_backlog.html', context)

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="mypdf.pdf"'
        return response



def reporte_horas_trabajadas(request, pk):
    proyecto = get_object_or_404(Proyecto, pk=pk)

    sprint_list = list()
    for sprint in proyecto.sprint_set.all().order_by('numero'):
        if sprint.estado != 1:
            sprint_list.append({
                'sprint': sprint,
                'tiempo': (sprint.fechaFinal - sprint.fechaInicio).days
            })



    context = {
        'sprints' : sprint_list,
        'proyecto': proyecto
    }

    html_string = render_to_string('reportes/pdf_template_horas_trabajadas.html', context)

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="mypdf.pdf"'
        return response

    return render(request,'reportes/pdf_template_horas_trabajadas.html',context=context)


def reporte_kanban_actual(request, pk):
    proyecto = get_object_or_404(Proyecto, pk=pk)

    context = dict()

    context['proyecto'] = proyecto
    context['flujos'] = Flujo.objects.filter(proyecto=proyecto).order_by('nombre')


    html_string = render_to_string('reportes/pdf_template_reporte_kanban.html', context)

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="mypdf.pdf"'
        return response

    return render(request, 'reportes/pdf_template_horas_trabajadas.html', context=context)