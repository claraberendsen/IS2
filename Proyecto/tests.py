from django.test import TestCase
from django.urls import reverse
from Accounts.models import User
from .models import Proyecto
from guardian.shortcuts import assign_perm
from django.contrib import auth
from django.contrib.auth.models import Group


# Create your tests here.
class TeamCreateViewTests(TestCase):
    def setup(self):
        """
        Setup para algunos tests en donde se deben estar con un usuario valido conectado al cliente
        """
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

       # assign_perm('Proyecto.crear', usuario)
        assign_perm('Proyecto.crear_proyecto', usuario)
        assign_perm('Proyecto.ver_todos_proyectos', usuario)
        # assign_perm('auth.modificar_proyecto', usuario)
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_crear_nuevo_proyecto(self):
        """
        Setup inicial del test y luego se prueba crear un proyecto enviando una solicitud Post para crear un nuevo proyecto
         llamado  IS2, el test es valido si se redirige a la lista de proyectos y el poryecto IS@ se encuentra en la lista
        """
        self.setup()
        response = self.client.post(reverse('Proyecto:crearProyecto'), data={'nombre': 'IS2', 'fechainicio': '2018-04-18'
                                            , 'fechafin': '2018-04-06'}, follow=True)

        self.assertContains(response, "IS2", msg_prefix="Error en proyecto/test_crear_nuevo_proyecto")

    def test_acceder_sin_permiso(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        response = self.client.post(reverse('Proyecto:crearProyecto'),
                                    data={'nombre': 'IS2','fechainicio': '2018-04-18', 'fechafin': '2018-04-06'},
                                    follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en proyecto/test_acceder_sin_permiso")

class ProyectoListarViewTest(TestCase):

    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_listar_proyecto(self):
        """
        Se crea un proyecto y se verica el correcto funcionamiento de la funcion listar
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.ver_todos_proyectos', usuario)
        Proyecto.objects.create(nombre='IS2', fechafin='2018-04-18', fechainicio='2018-04-06')
        response = self.client.get(reverse('Proyecto:listaProyectos'), follow=True)

        self.assertContains(response, 'IS2', msg_prefix="Error en proyecto/test_listar_proyecto")
    # def test_listar_proyecto_sin_permiso(self):
    #     """
    #     Se crea un proyecto y se intenta listar sin en el permiso correspodiente se verifica que el sistema retorne
    #     codigo 403
    #     """
    #     self.login()
    #     usuario = auth.get_user(self.client)
    #     Proyecto.objects.create(nombre='IS2')
    #     response = self.client.get(reverse('Proyecto:listaProyectos'), follow=True)
    #
    #     self.assertEqual(response.status_code, 403, msg="Error en proyecto/test_listar_proyecto_sin_permiso")

class ProyectoModificarViewTest(TestCase):

    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_modificar_sin_permiso(self):
        """
        Test de acceder a la página sin el permiso correspondiente
        :return:
        """
        self.login()
        Proyecto.objects.create(nombre='IS2', fechafin='2018-04-18', fechainicio='2018-04-06')
        response = self.client.get(reverse('Proyecto:modificarProyecto',kwargs={'pk': 1}), follow=True)

        self.assertEqual(response.status_code,403, msg="Error en proyecto/test_modificar_sin_permiso")

    def test_modificar_proyecto(self):
        """
        Se crea un usuario al cual se le asignan el permiso correspondiente para modificar un proyecto, se crea el
        proyecto a traves del metedo implicito en el modelo, se verifica que el proyecto ha sido creando, revisando el
        contenido de listaProyectos y se procede a modificarlo, se verifica si el proyecto contiene la frase IS3 y ya no
        contiene la frase IS2 para asegurase que el test fue exitoso
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        assign_perm('Proyecto.ver_todos_proyectos', usuario)
        id = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-18', fechainicio='2018-04-06').id

        response = self.client.get(reverse('Proyecto:listaProyectos'), follow=True)
        self.assertContains(response, "IS2")

        response = self.client.post(reverse('Proyecto:modificarProyecto',kwargs={'pk': id}), data={'nombre':'IS3',
                                            'fechainicio': '2018-04-18', 'fechafin': '2018-04-06'},follow=True)

        self.assertContains(response,'IS3', msg_prefix="Error en proyecto/test_modificar_proyecto")
        self.assertNotContains(response,'IS2', msg_prefix="Error en proyecto/test_modificar_proyecto")
