from django import forms
from datetime import datetime
from django.forms import DateField

from IS2 import settings
from Accounts.models import Rol, User
from Cliente.models import Cliente
from .models import Proyecto



class ProyectoForm(forms.ModelForm):
    nombre = forms.CharField(label='nombre', max_length=200,
                               widget=forms.TextInput(attrs={'class':'form-control', 'name':'nombre', }))

    descripcion = forms.CharField(label='descripcion', max_length=500,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'descripcion', }))

    # fechainicio = forms.DateField(label='fechainicio',
    #                                   widget=forms.DateInput(
    #                                       attrs={'class': ' form-control datepicker', 'name': 'fechainicio'}))
    #
    # fechafin = forms.DateField(label='fechafin',
    #                                widget=forms.DateInput(
    #                                    attrs={'class': ' form-control datepicker', 'name': 'fechafin'}))

    user = forms.ModelChoiceField(queryset=User.objects.all().exclude(username='AnonymousUser'), widget=forms.Select())

    horas_trabajadas = forms.DecimalField(label='horas_trabajadas', min_value=0.00,
                                          widget=forms.NumberInput(
                                              attrs={'class': 'form-control', 'name': 'horas_trabajadas'}))

    rol = forms.ModelChoiceField(queryset=Rol.objects.all().exclude(isProyectoRol=False))

    cliente = forms.ModelChoiceField(queryset=Cliente.objects.all(), required=False)


    # def clean(self):
    #     inicio = self.cleaned_data['fechainicio']
    #     fin = self.cleaned_data['fechafin']
    #     # if inicio < datetime.now.:
    #     #     raise forms.ValidationError("Las fecha de inicio es invalida")
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")    # def clean(self):
    #     inicio = self.cleaned_data['fechainicio']
    #     fin = self.cleaned_data['fechafin']
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")

    class Meta:
        model = Proyecto
        fields = [
            'nombre',
            'descripcion',
            # 'team'
        ]

class ProyectoModForm(forms.ModelForm):
    nombre = forms.CharField(label='nombre', max_length=10,
                               widget=forms.TextInput(attrs={'class':'form-control', 'name':'nombre', }))

    descripcion = forms.CharField(label='descripcion', max_length=10,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'descripcion', }))

    # fechainicio = forms.DateField(label='fechainicio',
    #                                   widget=forms.DateInput(
    #                                       attrs={'class': ' form-control datepicker', 'name': 'fechainicio'}))
    #
    # fechafin = forms.DateField(label='fechafin',
    #                                widget=forms.DateInput(
    #                                    attrs={'class': ' form-control datepicker', 'name': 'fechafin'}))

    cliente = forms.ModelChoiceField(queryset=Cliente.objects.all(), required=False)
    # team = forms.ModelMultipleChoiceField(queryset=Proyecto.objects.all(),
    #                                       widget=FilteredSelectMultiple("Teamselect", is_stacked=False))

    # def clean(self):
    #     inicio = self.cleaned_data['fechainicio']
    #     fin = self.cleaned_data['fechafin']
    #     # if inicio < datetime.now.:
    #     #     raise forms.ValidationError("Las fecha de inicio es invalida")
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")    # def clean(self):
    #     inicio = self.cleaned_data['fechainicio']
    #     fin = self.cleaned_data['fechafin']
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")
    class Meta:
        model = Proyecto
        fields = [
            'nombre',
            'descripcion',
            'fechainicio',
            'fechafin',

        ]
