
from django.urls import path,include
from . import views
from django.conf.urls import url
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from guardian.decorators import permission_required_or_403


# Urls del sistema
app_name = 'Proyecto'
urlpatterns = [
    path('', views.ListaProyectos.as_view(), name='listaProyectos'),
    path('crear/', views.Crear_Proyecto.as_view(), name='crearProyecto'),
    path('<int:pk>/modificar/', views.Modificar_Proyecto.as_view(), name='modificarProyecto'),
    path('<int:pk>/home/', views.HomeProyectos.as_view(), name='homeProyecto'),

    path('<int:pk>/modificar/team/', include('team.urls')),
    path('<int:pk>/sprint/', include('Sprint.urls')),
    path('<int:pk>/kanban/', include('Kanban.urls')),
    path('<int:pk>/modificar/userstories/', include('UserStory.urls')),
    path('projects/list', views.ListaProyectos, name='pro_list'),
    path('proyecto/crear', views.Crear_Proyecto.as_view(), name='pro_crear'),
    path('<int:pk>/confirmar_terminar', views.VerificarTerminarProyecto.as_view(), name='confirmar_terminarProyecto'),
    path('<int:pk>/terminar', views.CancelarProyecto, name='terminar'),
    path('<int:pk>/iniciar', views.iniciarProyectoView, name='iniciarProyecto'),

    path('<int:pk>/reporte/product_backlog', views.reporte_product_backlog, name='reporteProductBacklog'),
    path('<int:pk>/reporte/horas_totales', views.reporte_horas_trabajadas, name='reporteHorasTrabajadas'),
    path('<int:pk>/reporte/kanban', views.reporte_kanban_actual, name='reporteKanban'),

    # url(r'^team/', include('team.urls', namespace='Team'))
]
