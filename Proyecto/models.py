from django.db import models
from datetime import datetime
from django.contrib import admin
from Cliente.models import Cliente
from Accounts.models import User
from auditlog.registry import auditlog

estados = (
    (1, 'Planificado'),
    (2, 'En Ejecución'),
    (3, 'Terminado'),
    (4, 'Cancelado')
)

# Create your models here.
class Proyecto(models.Model):
    nombre          = models.CharField(max_length=200)
    descripcion      =models.TextField(max_length=500)
    #team            = models.ManyToManyField(User, blank=True )
    fechainicio      = models.DateField(null=True, blank=True)
    fechafin        = models.DateField(null=True, blank=True)
    timestamp       = models.DateTimeField(auto_now_add=True)
    cliente = models.ForeignKey(Cliente, on_delete=models.DO_NOTHING, null=True, blank=True)
    estado = models.IntegerField(choices=estados, default=1)  # Estados 1



    def __str__(self):
        return str(self.nombre)

    class ProyectoAdmin(admin.ModelAdmin):
        filter_horizontal = ('team')
    class Meta:
        default_permissions = []
        permissions = (
            ("ver_todos_proyectos", "Ver todos los proyectos"),
            ("modificar_proyecto", "Modificar proyectos"),
            ("crear_proyecto", "Crear proyectos"),
            ("iniciar_proyecto", "Iniciar proyectos"),
            ("eliminar_proyecto", "Terminar proyectos"),
        )

auditlog.register(Proyecto)