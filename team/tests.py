from django.test import TestCase
from django.urls import reverse
from Accounts.models import User, Rol
from Proyecto.models import Proyecto
from team.models import team
from guardian.shortcuts import assign_perm
from django.contrib import auth
from django.contrib.auth.models import Group


# Create your tests here.
class TeamCreateViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_agregar_nuevo_miembro(self):
        """
        Setup inicial del test y luego se prueba agregar un miembro, a un proyecto ya existente para esto se le brinda,
        al usuario los permisos adecuados(modificar_proyecto y crear_team) mediante una llamada post y de parametro el
        id del proyecto
        """
        self.login()
        usuario = auth.get_user(self.client)
        usuario2 = User(username='jorge', email="abcd@gmail.com")
        usuario2.set_password('123456')
        usuario2.save()


        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('team.crear_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06', fechainicio='2018-04-05')

        member = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)

        response = self.client.post(reverse('Proyecto:Team:crearTeam', kwargs={'pk':proyecto.id}), data={'user': 'usuario2',
                                    'horas': '5.00', 'rol': rol}, follow=True)

        self.assertContains(response, usuario2.username, msg_prefix="Error en team/Test_agregar_nuevo_miembro" )

    def test_agregar_miembro_sin_permiso(self):
        """
        Setup inicial y luego se procede a intentar agregar un miembro al team  de un proyecto ya existente, se le brinda
        al usuario el permiso de modificar_proyecto sin embargo no el de crear_team por lo tanto la llamada post retorna
        un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)
        usuario2 = User(username='jorge', email="abcd@gmail.com")
        usuario2.set_password('123456')
        usuario2.save()

        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-07', fechainicio='2018-04-06')

        member = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)

        response = self.client.post(reverse('Proyecto:Team:crearTeam', kwargs={'pk': proyecto.id}),
                                data={'user': 'usuario2',
                                      'horas': '5.00', 'rol': rol}, follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en team/test_agregar_miembro_sin_permiso")


class ListarTeamViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_listar_miembros(self):
        """
         Setup inicial y luego se procede a intentar listar los miembros del team  de un proyecto ya existente, se le brinda
         al usuario el permiso de listar_proyecto sin embargo no el de modifcar_team por lo tanto la llamada post retorna
         un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('team.listar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-08',
                                           fechainicio='2018-04-06')

        pkt = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol= rol).id
        response = self.client.get(reverse('Proyecto:Team:listaTeam', kwargs={'pk': proyecto.id}), follow=True)
        self.assertContains(response, usuario.username, msg_prefix="Error en team/test_listar_miembros")


    def test_listar_miembros_sin_permiso(self):
        """
         Setup inicial y luego se procede a intentar listar los miembros del team  de un proyecto ya existente, se le brinda
         al usuario el permiso de modificar_proyecto sin embargo no el de listar_team por lo tanto la llamada post retorna
         un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        #assign_perm('team.listar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-09',
                                           fechainicio='2018-04-06')

        pkt = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol= rol).id
        response = self.client.get(reverse('Proyecto:Team:listaTeam', kwargs={'pk': proyecto.id}), follow=True)
        self.assertEqual(response.status_code, 403, msg="Error en team/test_listar_miembros_sin_permiso")

class ModificarTeamViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abcr@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_modificar_miembro(self):
        """
        Setup inicial del test y luego se prueba modificar un miembro del team de un proyecto ya existente para esto
         se le brinda,al usuario los permisos adecuados(modificar_proyecto y modificar_team) mediante una llamada post
         y de parametro el id del proyecto, el campo a ser modificado, y nos aseguramos que el dato ha sido modificado
         como se espera con assertContains
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('team.modificar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-09', fechainicio='2018-04-06')

        pkt = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol=rol).id
        response=self.client.post(reverse('Proyecto:Team:modificarTeam', kwargs={'pk': proyecto.id, 'pkt': pkt}),
                                  data={'horas_trabajadas': 6.00}, follow=True)
        self.assertContains(response, 6.00, msg_prefix="Error en team/test_modificar_miembro")

    def test_modificar_miembro_sin_permiso(self):
        """
       Setup inicial y luego se procede a intentar modifcar un miembro del team  de un proyecto ya existente, se le brinda
       al usuario el permiso de modificar_proyecto sin embargo no el de modifcar_team por lo tanto la llamada post retorna
       un codigo http 403
       :return:
       """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
       # assign_perm('team.modificar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-08', fechainicio='2018-04-06')

        pkt = team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol=rol).id
        response=self.client.post(reverse('Proyecto:Team:modificarTeam', kwargs={'pk': proyecto.id, 'pkt': pkt}),
                                  data={'horas_trabajadas': 6.00}, follow=True)
        self.assertEqual(response.status_code, 403, msg="Error en team/test_modificar_miembro_sin_permiso")

class EliminarTeamViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email="abc@gmail.com")
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_eliminar_miembro(self):
        """
        Setup inicial del test y luego se prueba eliminar un miembro del team de un proyecto ya existente para esto
        se le brinda,al usuario los permisos adecuados(modificar_proyecto y eliminar_team) mediante una llamada post
        y de parametro el id del proyecto, el id del miembro a ser borrado, y nos aseguramos que el miembro ha sido borrado
        como se espera con assertContains
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        usuario2 = User(username='jorge', email="abecd@gmail.com")
        usuario2.set_password('123456')
        usuario2.save()

        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('team.eliminar_team', rol)
        assign_perm('team.modificar_team', rol)
        assign_perm('team.listar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-09', fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol=rol)
        pkt = team.objects.create(proyecto=proyecto, user=usuario2, horas_trabajadas=5.00, rol=rol).id
        response = self.client.post(reverse('Proyecto:Team:eliminarTeam', kwargs={'pk': proyecto.id, 'pkt': pkt}),
                                                                                             follow=True)

        self.assertNotContains(response, 'jorge', msg_prefix="Error en team/test_eliminar_miembro")

    def test_eliminar_miembro_sin_permiso(self):
        """
        Setup inicial y luego se procede a intentar eliminar un miembro del team  de un proyecto ya existente, se le brinda
        al usuario el permiso de modifiar_proyecto sin embargo no el de eliminar_team por lo tanto la llamada post retorna
        un codigo http 403
        :return:
        """
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        usuario2 = User(username='jorge', email="abcd@gmail.com")
        usuario2.set_password('123456')
        usuario2.save()

        rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)
        assign_perm('team.modificar_team', rol)

        proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-18', fechainicio='2018-04-06')

        team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=5.00, rol=rol)
        pkt = team.objects.create(proyecto=proyecto, user=usuario2, horas_trabajadas=5.00, rol=rol).id
        response = self.client.post(reverse('Proyecto:Team:eliminarTeam', kwargs={'pk': proyecto.id, 'pkt': pkt}),
                                                                                             follow=True)

        self.assertEqual(response.status_code, 403, msg="Error en team/test_eliminar_miembro_sin_permiso")