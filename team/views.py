from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from django.http import Http404
from django.contrib.auth.mixins import PermissionRequiredMixin
from guardian import mixins
from team.forms import TeamForm
from team.models import team
# Login view

from django.views.generic.edit import CreateView, UpdateView

# Create your views here.
from Accounts.models import User
from .models import team
from .forms import TeamForm
from Proyecto.models import Proyecto

class Crear_team(mixins.PermissionRequiredMixin, CreateView):
    """
       Crear_team hereda de de la vista generica CreateView y PermissionRequiredMixin
       Requiere que el cliente possee el  permiso crear_team para poder ser utilizada
    """
    model = team
    template_name = 'team/team_create.html'
    form_class  = TeamForm

    permission_required = 'team.crear_team'
    raise_exception = True

    def get_form(self, form_class=None):
        """
        Se actualiza el queryset del form
        :param form_class:
        :return:
        """
        form = super().get_form(form_class)
        users = team.objects.filter(proyecto=self.kwargs['pk'])
        lista = []
        for us in users:
            lista.append(us.user.id)
        form.fields['user'].queryset = User.objects.filter().exclude(id__in=lista)
        return form

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def form_valid(self, form):
        """
        form_valid permite alterar el form generico por defecto que acompanha a CreateView
        :param form, self: el form generico
        :return : el form generico modificado por form_valid
        """
        proyec = self.kwargs['pk']
        form.instance.proyecto = Proyecto.objects.filter(id=proyec).first()

        # user = form.instance.user
        # proyecto = Proyecto.objects.get(pk=proyec)
        # rol = Group.objects.get(pk=form.instance.rol2.id)
        # for perm in rol.permissions.all():
        #     assign_perm(perm, user, proyecto)

        return super(Crear_team, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Team:listaTeam',
                            kwargs={'pk': self.kwargs['pk']})


class Modificar_team(mixins.PermissionRequiredMixin, UpdateView):
    """
       Modificar_team hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el cliente possee el  permiso modificar_team para poder ser utilizada
    """
    model = team
    # fields = ['horas_trabajadas','rol']
    # template_name_suffix = '_update_form'
    pk_url_kwarg = 'pkt'
    template_name = 'team/team_mod.html'
    form_class = TeamForm
    permission_required = 'team.modificar_team'
    raise_exception = True

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyecto'] =  get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        return context

    def get(self, request, *args,  **kwargs):
        pkp = self.kwargs['pk']
        if team.objects.get(id=self.kwargs['pkt']).proyecto.id == pkp:
            return  super().get(self, request, *args, **kwargs)
        else:
            raise Http404("No tienes permiso para ver esta pagina")

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Un objeto team
        """
        return team.objects.filter(id=self.kwargs['pkt'])

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Team:listaTeam',
                            kwargs={'pk': self.kwargs['pk']})

    def post(self, request, *args, **kwargs):
        """
        Se define la funcion post para detectar cuando se intenta eliminar un elemento y  redireccionar el pedido a
        la url Proyecto;Team:eliminarTeam
        :param request:
        :param args:
        :param kwargs: Parametros necesarios para la view EliminarTeam
        :return: La funcion post de la clase parent
        """
        if 'delete' in request.POST:
            return redirect('Proyecto:Team:eliminarTeam', **kwargs)
        return super().post(request, *args, **kwargs)



    def get(self, request, *args, **kwargs):
        pkp = self.kwargs['pk']
        if team.objects.get(id=self.kwargs['pkt']).proyecto.id == pkp:
            return super().get(self,request,*args,**kwargs)
        else:
            raise Http404("Hacker afuera!")




class Listateam(mixins.PermissionRequiredMixin, generic.ListView):
    """
    Listateam hereda de de la vista generica ListView y PermissionRequiredMixin
    Requiere que el cliente possee el  permiso listar_team para poder ser utilizada
    """
    model = team
    template_name = 'team/team_listar.html'

    permission_required = 'team.listar_team'
    raise_exception = True


    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)
    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] =  get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Un objeto team
        """
        return team.objects.filter(proyecto=self.kwargs['pk'])


class Eliminarteam(mixins.PermissionRequiredMixin, generic.DeleteView):
    """
    Eliminarteam hereda de de la vista generica DeleteView y PermissionRequiredMixin
    Requiere que el cliente possee el  permiso eliminar_team para poder ser utilizada
    """
    model = team
    pk_url_kwarg = 'pkt'
    template_name = 'team/team_listar.html'
    permission_required = 'team.eliminar_team'
    raise_exception = True

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] =  get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get(self, request, *args, **kwargs):
        pkp = self.kwargs['pk']
        if team.objects.get(id=self.kwargs['pkt']).proyecto.id == pkp:
            return super().get(self, request, *args, **kwargs)
        else:
            raise Http404("No tienes permiso para ver esta pagina")

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        #self.template_name = 'team/team_confirm_delete.html'
        return reverse_lazy('Proyecto:Team:listaTeam',
                            kwargs={'pk': self.kwargs['pk']})

