from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from Proyecto.models import Proyecto
from team.models import team
from UserStory.models import UserStory
from auditlog.registry import auditlog
# Create your models here.
estados = (
    (1, 'Planificado'),
    (2, 'En Ejecución'),
    (3, 'Terminado'),
    (4, 'Cancelado')
)

class Sprint(models.Model):
    fechaInicio = models.DateField(null=True, blank=True)
    fechaFinal = models.DateField(blank=True, null=True)
    fechaFinalCalculada = models.DateField(null=True,blank=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT)
    estado = models.IntegerField(choices=estados, default=1) #Estados 1
    userStories = models.ManyToManyField(UserStory)
    duracion = models.IntegerField(null=True, blank=True)
    numero = models.IntegerField(default=0)
    reunion = models.TextField(blank=True)



    class Meta:
        default_permissions = []
        permissions = (
            ("listar_sprint", "Ver lista de Sprints"),
            ("modificar_sprint", "Modificar Sprints"),
            ("crear_sprint", "Crear Sprints"),
            ("eliminar_sprint", "Eliminar Sprints"),
            ("ver_sprint", "Ver Sprint"),
            ("iniciar_sprint", "Iniciar sprints"),
            ("importar_sprint", "Importar Sprints")
        )
        unique_together = ('proyecto', 'numero',)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.numero == 0:
            try:
                n = Sprint.objects.filter(proyecto=self.proyecto).order_by('-numero')[0].numero
                self.numero = n + 1
            except (ObjectDoesNotExist, IndexError):
                self.numero = 1
        super(Sprint, self).save(force_insert, force_update, using, update_fields)


    def __str__(self):
        return 'Sprint '+str(self.numero) + '. Proyecto: ' + self.proyecto.nombre

class SprintUserMember(models.Model):
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    member = models.ForeignKey(team, on_delete=models.CASCADE)
    horasDedicadas = models.IntegerField()

    class Meta:
        default_permissions = []

auditlog.register(Sprint)
