
from django.urls import path
from . import views
from UserStory.views import  Redefinir_User_Story
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from guardian.decorators import permission_required_or_403


# Urls del sistema
app_name = 'sprint'
urlpatterns = [
    path('', views.Lista_Sprint.as_view(), name='listaSprint'),
    path('crear/', views.Crear_Sprint.as_view(), name='crearSprint'),
    path('<int:pks>/modificar/', views.Modificar_Sprint.as_view(), name='modificarSprint'),
    path('<int:pks>/modificarFechas/', views.ModificarFechaSprint.as_view(), name='modificarFechasSprint'),
    path('<int:pks>/eliminar/', views.Eliminar_Sprint.as_view(), name='eliminarSprint'),
    path('<int:pks>/cancelar/', views.CancelarSprint.as_view(), name='terminarSprint'),
    path('<int:pks>/iniciar/', views.iniciarSprintView, name='iniciarSprint'),
    path('importar', views.Importar_Sprint.as_view(), name='importarSprint'),
    path('<int:pks>/seleccionar_importar/<int:pkp>', views.importarSprintView, name='importarS'),
    path('<int:pks>/agregarUS/', views.Agregar_US_A_Sprint, name='agregarUS'),
    path('<int:pks>/modificar/<int:pku>/reasignar/', views.Reasignar_User_Story.as_view(), name='reasignarUS'),
    path('<int:pks>/modificar/<int:pku>/remover/', views.Remover_US_De_Sprint, name='removerUS'),
    path('<int:pks>/modificar/<int:pku>/redefinir/', Redefinir_User_Story.as_view(), name='redefinirUS'),
    path('<int:pks>/reporte/US_prioridad_max', views.reporte_US, name='reporteUSpendientes'),
    path('<int:pks>/reporte/Sprint_Backlog', views.reporte_sprint_backlog, name='reporteSprintBacklog'),

]
