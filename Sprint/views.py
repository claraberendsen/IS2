import decimal
from datetime import timedelta, date, timezone, datetime

from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views.generic import UpdateView, ListView, DeleteView, CreateView
from django.views import View
from formtools.wizard.views import SessionWizardView
from guardian import mixins
from guardian.decorators import permission_required_or_403
from guardian.mixins import PermissionRequiredMixin

from Kanban.models import Fase, Flujo
from Proyecto.models import Proyecto
from Sprint.models import Sprint, SprintUserMember
from UserStory.models import UserStory, UserStoryHistory
from Kanban.models import Flujo, Fase
from team.models import team
from Accounts.models import User
from .forms import CrearSprintForm, CrearSprintForm2, UpdateSprintForm,  Cancelar
from guardian.mixins import PermissionRequiredMixin
from guardian import mixins
from guardian.decorators import permission_required_or_403
from django.forms import formset_factory
from datetime import timedelta
from UserStory.tasks import us_desasignado_task

from weasyprint import HTML


class Crear_Sprint(mixins.PermissionRequiredMixin, CreateView):
    model = Sprint
    form_class = CrearSprintForm
    template_name = 'Sprint/sprint_form2.html'
    permission_required = 'Sprint.crear_sprint'
    raise_exception = True


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])

    def done(self, form_list, **kwargs):
        """
        El ultimo paso de django-formtools, es llamda cuando ya se ha pasado por todas las paginas del form y se utiliza
        para guardar en almacenamiento permanente la informacion recabada.
        :param form_list:
        :param kwargs:
        :return HttpRedirect:
        """
        for form in form_list:
            form.instance.proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
            s = form.save()
            break

        sprint = Sprint.objects.get(pk=s.pk)


        horasEstimadasUserStories = 0
        for us in sprint.userStories.all():
            us.encargado = team.objects.get(pk=self.storage.data['step_data']['1'][str(us.id)][0])
            us.save()

        horasTotal = 0
        # for t in team.objects.filter(proyecto=self.kwargs['pk']):
        #     SprintUserMember.objects.create(sprint=sprint, member=t, horasDedicadas=self.storage.data['step_data']['2'][str(t.id)][0])

        # calcularFechaFinal(sprint)

        return HttpResponseRedirect(self.get_success_url())


    def form_valid(self, form):
        form.instance.proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        self.object = form.save()
        sprint = Sprint.objects.get(pk=self.object.pk)
        for us in sprint.userStories.all():
            us.encargado = team.objects.get(pk=self.request.POST[str(us.id)])
            mail_subject = 'Asignacion de User Story'
            message = render_to_string('Sprint/notif_asignacion.html', {
                'user': us.encargado.user,
                'us': us
            })
            us.encargado.user.email_user(mail_subject, message)
            us.save()
        for t in team.objects.filter(proyecto=self.kwargs['pk']):

            SprintUserMember.objects.create(sprint=sprint, member=t, horasDedicadas=self.request.POST['horas_'+str(t.id)])



        return HttpResponseRedirect(self.get_success_url())


    def get_form(self):
        form = super(Crear_Sprint, self).get_form()



        form.fields['userStories'].queryset = UserStory.objects.filter(proyecto=self.kwargs['pk']).filter(sprint=None)

        return form

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:listaSprint', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs pk:
        :return El nuevo context:
        """
        context = super().get_context_data(**kwargs)
        context['proyecto'] = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['team'] = team.objects.filter(proyecto=self.kwargs['pk'])
        suma = 0
        sprintusermember = []
        for t in team.objects.filter(proyecto=self.kwargs['pk']):
            suma += t.horas_trabajadas

        context['horasTeam'] = suma
        context['userstories'] = UserStory.objects.filter(proyecto=self.kwargs['pk']).filter(sprint=None)


        return context



class Modificar_Sprint(mixins.PermissionRequiredMixin, ListView):
    model = UserStory
    template_name = 'Sprint/sprint_us_listar.html'
    permission_required = 'Sprint.modificar_sprint'
    raise_exception = True

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return Una lista de objetos UserStory:
        """
        return UserStory.objects.filter(sprint=self.kwargs['pks'])

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        pkp = self.kwargs['pk']
        return get_object_or_404(Proyecto, pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = get_object_or_404(Proyecto,pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        sprint = get_object_or_404(Sprint, pk=self.kwargs['pks'])
        context['sprint'] = sprint

        etiquetas = []
        horas = []
        horas_estimadas = 0
        var_date = sprint.fechaInicio


        horast = 0
        for t in sprint.proyecto.team_set.all():
            horast += t.horas_trabajadas

        progreso_estimado = []

        lista = []
        q = UserStory.objects.filter(sprint=self.kwargs['pks'])
        for us in q:
            horas_estimadas = horas_estimadas + us.tiempo_estimado_de_terminacion
            lista.append(us.id)
        qs = UserStoryHistory.objects.filter(id_us__in=lista)


        aux2 = horas_estimadas


        progreso_estimado.append(horas_estimadas)


        i = 1
        if sprint.estado != 2 and sprint.estado != 1:
            while( var_date <= sprint.fechaFinal ):
                aux = 0

                etiquetas.append(var_date)

                qs = UserStoryHistory.objects.filter(id_us__in=lista).order_by("fecha_modificacion")
                ush_anterior = None
                anterior = None
                for ush in qs:
                    if ush.fecha_modificacion.date() == (var_date):

                        ush_anterior = UserStoryHistory.objects.filter(id_us=ush.id_us). \
                            filter(fecha_modificacion__date__lt=ush.fecha_modificacion.date()). \
                                order_by("-fecha_modificacion").first()

                            # ush_anterior = query[0]
                        if anterior == None:
                            anterior = ush
                            if ush_anterior == None:
                                aux += ush.horas_trabajadas
                            else:
                                aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas
                        else:
                            if anterior.id_us == ush.id_us and \
                                    anterior.fecha_modificacion.date() == ush.fecha_modificacion.date():
                                if ush_anterior == None:
                                    aux = aux + ush.horas_trabajadas - anterior.horas_trabajadas
                                else:
                                    aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas -(anterior.horas_trabajadas - ush_anterior.horas_trabajadas)

                                anterior = ush
                            else:
                                if ush_anterior == None:
                                    aux += ush.horas_trabajadas
                                else:

                                    aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas
                                anterior = ush

                aux2 -= aux
                horas.append(aux2)
                if horas_estimadas-horast*i >= 0:
                    progreso_estimado.append(horas_estimadas-horast*i)
                else:
                    progreso_estimado.append(0)
                i += 1
                var_date += timedelta(days=1)

        elif sprint.estado != 1:

            while(var_date <= date.today()):
                aux = 0

                etiquetas.append(var_date)
                qs = UserStoryHistory.objects.filter(id_us__in=lista).order_by("fecha_modificacion")
                anterior = None

                for ush in qs:
                    if  ush.fecha_modificacion.date() == var_date:

                        ush_anterior = UserStoryHistory.objects.filter(id_us=ush.id_us). \
                            filter(fecha_modificacion__date__lt=ush.fecha_modificacion.date()). \
                            order_by("-fecha_modificacion").first()

                        if anterior == None:
                            anterior = ush
                            if ush_anterior == None:
                                aux += ush.horas_trabajadas
                            else:
                                aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas
                        else:
                            if anterior.id_us == ush.id_us and \
                                    anterior.fecha_modificacion.date() == ush.fecha_modificacion.date():
                                if ush_anterior == None:

                                    aux = aux + ush.horas_trabajadas - anterior.horas_trabajadas

                                else:
                                    aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas - (
                                                anterior.horas_trabajadas - ush_anterior.horas_trabajadas)

                                anterior = ush
                            else:
                                if ush_anterior == None:
                                    aux += ush.horas_trabajadas
                                else:
                                    aux = aux + ush.horas_trabajadas - ush_anterior.horas_trabajadas
                                anterior = ush

                aux2 -= aux
                horas.append(aux2)
                if horas_estimadas - horast * i >= 0:
                    progreso_estimado.append(horas_estimadas - horast * i)
                else:
                    progreso_estimado.append(0)
                i = i + 1
                var_date += timedelta(days=1)

        context['etiquetas'] = etiquetas
        context['horas_estimadas'] = horas_estimadas
        context['horas_trabajadas'] = horas

        context['progreso_estimado'] = progreso_estimado

        return context




class Lista_Sprint(mixins.PermissionRequiredMixin, ListView):
    model = Sprint
    permission_required = 'Sprint.listar_sprint'
    raise_exception = True

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        pkp = self.kwargs['pk']
        return get_object_or_404(Proyecto,pk=pkp)


    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return Una lista de objetos UserStory:
        """
        return Sprint.objects.all().filter(proyecto_id=self.kwargs['pk']).order_by('numero')

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        sprints = Sprint.objects.all().filter(proyecto_id=self.kwargs['pk'])
        ejecucion = False
        for s in sprints:
            if s.estado == 2:
                ejecucion = True


        context = super().get_context_data(**kwargs)

        # context['lista_importados']= Sprint.objects.all().exclude(proyecto_id=self.kwargs['pk'])

        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        context['ejecucion'] = ejecucion
        return context



class Eliminar_Sprint(mixins.PermissionRequiredMixin, DeleteView):
    model = Sprint
    pk_url_kwarg = 'pks'
    permission_required = 'Sprint.eliminar_sprint'
    raise_exception = True

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:listaSprint', kwargs={'pk': self.kwargs['pk']})

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        pkp = self.kwargs['pk']
        return get_object_or_404(Proyecto, pk=pkp)

class Reasignar_User_Story(mixins.PermissionRequiredMixin, UpdateView):
    model = UserStory
    pk_url_kwarg = 'pku'
    template_name = 'Sprint/reasignarUS.html'
    fields = ['encargado']
    permission_required = 'Sprint.modificar_sprint'
    raise_exception = True



    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])

    def get_form(self, form_class=None):
        """
        Se actualiza el queryset del form
        :param form_class:
        :return:
        """
        form = super().get_form(form_class)
        form.fields['encargado'].queryset = team.objects.filter(proyecto=self.kwargs['pk'])
        return form

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        sprint = get_object_or_404(Sprint, pk=self.kwargs['pks'])
        context['proyectoKey'] = self.kwargs['pk']
        context['sprint'] = sprint
        context['proyecto'] = proyecto
        return context




    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:modificarSprint',
                            kwargs={'pk': self.kwargs['pk'], 'pks': self.kwargs['pks']})


    def form_valid(self, form):
        self.object = form.save()
        us = self.object
        us_desasignado_task.delay(us.encargado.user_id, us.pk)
        mail_subject = 'Asignacion de User Story'
        message = render_to_string('Sprint/notif_asignacion.html', {
            'user': us.encargado.user,
            'us': us
        })
        us.encargado.user.email_user(mail_subject, message)
        us.save()



        return HttpResponseRedirect(self.get_success_url())


class CancelarSprint(mixins.PermissionRequiredMixin, UpdateView):
    model = Sprint
    pk_url_kwarg = 'pks'
    form_class = Cancelar
    template_name = 'Sprint/us_incompletos.html'
    permission_required = 'Sprint.modificar_sprint'
    raise_exception = True

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])


    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        sprint = get_object_or_404(Sprint, pk=self.kwargs['pks'])
        if sprint.estado == 2:
            sprint.estado = 3
            sprint.fechaFinal = date.today()
            sprint.save()
        context['proyectoKey'] = self.kwargs['pk']
        context['sprint'] = sprint
        context['proyecto'] = proyecto

        qs = UserStory.objects.filter(sprint=self.kwargs['pks']).exclude(estado=1)

        lista_id = []
        for us in qs:
            if us.fase_actual.orden != Fase.objects.filter(flujo=us.flujo).count() or us.estado_actual != 'Done':
                    lista_id.append(us.id)
                    if sprint.estado == 2 or sprint.estado == 3:
                        sprint.estado = 4
                        sprint.fechaFinal = date.today()
                        sprint.save()
        context['us_incompletos'] = UserStory.objects.filter(id__in=lista_id)
        return context


    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:listaSprint', kwargs={'pk': self.kwargs['pk']})



@permission_required_or_403('Sprint.modificar_sprint', (Proyecto, 'id', 'pk'))
def Remover_US_De_Sprint(request, pk, pks, pku):
    """
    Vista basada en funciones que recibe un mensaje post para eliminar un user story del sprint backlog
    :param request:
    :param pk:
    :param pks:
    :param pku:
    :return:
    """
    if request.method == 'POST':
        us = UserStory.objects.get(pk=pku)
        sprint = Sprint.objects.get(pk=pks)
        proyecto = Proyecto.objects.get(pk=pk)
        us_desasignado_task.delay(us.encargado.user_id, us.pk)

        sprint.userStories.remove(us)
        us.encargado = None
        us.estado_actual = None
        us.fase_actual = None
        us.save()
        if sprint.estado != 1:
            sprint.fechaFinalCalculada = calcularFechaFinal(sprint)
            sprint.save()

        return HttpResponseRedirect(reverse_lazy('Proyecto:sprint:modificarSprint',
                                                 kwargs={'pk': pk, 'pks': pks}))


@permission_required_or_403('Sprint.modificar_sprint', (Proyecto, 'id', 'pk'))
def Agregar_US_A_Sprint(request, pk, pks):
    """
    Vista basada en funcioens que recibe un mensaje get y muestra la pagina para agregar un US al sprint junto con su
    usuario encargado. El mensaje post contiene los datos elegidos y son guardados en la base de datos.
    :param request:
    :param pk:
    :param pks:
    :return:
    """

    proyecto =  get_object_or_404(Proyecto, pk=pk)
    sprint = get_object_or_404(Sprint, pk=pks)
    userStoriesYaAsignados = set(UserStory.objects.filter(proyecto=proyecto)) - set(UserStory.objects.filter(proyecto=proyecto).filter(sprint=None))
    userSotiresDisponibles = UserStory.objects.filter(proyecto=proyecto).filter(sprint=None)
    t = proyecto.team_set.all()
    if request.method == 'GET':
        return render(request, 'Sprint/sprint_nuevo_us.html', context={'proyecto': proyecto, 'sprint': sprint,
                                                                       'usDisponibles':userSotiresDisponibles,
                                                                       'usAsignados':userStoriesYaAsignados,
                                                                       'team':t,
                                                                       'proyectoKey':proyecto.id})

    if request.method == 'POST':
        idUserStory = request.POST['userstory']
        idEncargado = request.POST['encargado']

        us = UserStory.objects.get(pk=idUserStory)
        encargado = team.objects.get(pk=idEncargado)
        us.encargado = encargado

        # /MAILLL

        us.estado_actual = 'To do'
        us.fase_actual = us.flujo.faseaflujo.get(orden=1)
        us.save()
        sprint.userStories.add(us)
        sprint.save()
        if sprint.estado != 1:
            sprint.fechaFinalCalculada = calcularFechaFinal(sprint)
            sprint.save()

        return HttpResponseRedirect(reverse_lazy('Proyecto:sprint:modificarSprint',
                                                 kwargs={'pk': pk, 'pks': pks}))

class ModificarFechaSprint(PermissionRequiredMixin, UpdateView):
    model = Sprint
    form_class = UpdateSprintForm
    template_name = 'Sprint/sprint_update_fecha.html'
    permission_required = ['Sprint.modificar_sprint']
    raise_exception = True
    pk_url_kwarg = 'pks'


    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs pk:
        :return El nuevo context:
        """
        context = super(ModificarFechaSprint, self).get_context_data(**kwargs)
        context['proyecto'] = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        return context

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)



    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:listaSprint',
                            kwargs={'pk': self.kwargs['pk']})

    def form_valid(self, form):
        """
        Aplica los cambios y calcula la fecha final del sprint
        :param form:
        :return:
        """
        self.object = form.save()

        sprint = Sprint.objects.get(pk=self.kwargs['pks'])

        calcularFechaFinal(sprint)

        return HttpResponseRedirect(self.get_success_url())



def calcularFechaFinal(sprint, duracion=None):
    """
    Esta funcion calcula la fecha final del sprint, teniendo en cuenta dias habiles de lunes a viernes

    :param sprint: el sprint que se debe calcular su fecha final
    :param duracion: parametro opcional, si es None la fecha final se calcula en base a la duracion individual de cada
    sprint y disponibilidad del team, y si contiene un valor en base a la duracion requerida
    :return: la fecha final en formato Date
    """
    if duracion is None:
        horasus = 0
        for us in sprint.userStories.all():
            horasus += us.tiempo_estimado_de_terminacion

        horast = 0
        for t in sprint.proyecto.team_set.all():
            horast += t.horas_trabajadas

    try:
        current_date = sprint.fechaInicio
        if duracion is None:
            d = int(horasus / horast)
        else:
            d = duracion
        while d > 0:
            current_date += timedelta(days=1)
            weekday = current_date.weekday()
            if weekday >= 5:  # sunday = 6
                continue

            if current_date in feriados:
                continue
            d -= 1


    except (decimal.DivisionByZero, ZeroDivisionError) as e:
        return sprint.fechaFinal
    else:
        return current_date


feriados = [
    date(2018, 1, 1),
    date(2018, 3, 1),
    date(2018, 3, 29),
    date(2018, 3, 30),
    date(2018, 5, 1),
    date(2018, 5, 14),
    date(2018, 5, 15),
    date(2018, 6, 12),
    date(2018, 8, 15),
    date(2018, 9, 29),
    date(2018, 12, 8),
    date(2018, 12, 25)
]


class Importar_Sprint(mixins.PermissionRequiredMixin, ListView):
    model = Sprint
    template_name = 'Sprint/importar_sprint.html'
    permission_required = 'Sprint.importar_sprint'
    raise_exception = True

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        pkp = self.kwargs['pk']
        return get_object_or_404(Proyecto, pk=pkp)

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return Una lista de objetos UserStory:
        """
        return Sprint.objects.all().exclude(proyecto_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        context['lista_importados']= Sprint.objects.all().exclude(proyecto_id=self.kwargs['pk'])

        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        return context

    # def get_success_url(self):
    #     """
    #     Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
    #     :return: url de redireccionamiento
    #     """
    #     return reverse_lazy('Proyecto:sprint:importarS', kwargs={'pk': self.kwargs['pk'],
    #                                                              'pks': self.kwargs['pks'],
    #                                                              'pkp': self.kwargs['pkp']})

@permission_required_or_403('Sprint.importar_sprint', (Proyecto, 'id', 'pk'))
def importarSprintView(request, pk, pks, pkp):
    if request.method == 'POST':
        sprint = Sprint.objects.get(pk=pks)
        proyecto_actual = Proyecto.objects.get(pk=pkp)
        sprint2 = Sprint.objects.create(proyecto_id=proyecto_actual.pk, fechaInicio=None, fechaFinal=None,
                                        fechaFinalCalculada=None,
                                        estado=1, duracion=None)
        userstories = sprint.userStories.all()
        for us in userstories:
            us2 = us
            us2.pk = None
            us2.proyecto = proyecto_actual
            us2.estado = 1
            us2.estado_actual = 'To do'
            us2.encargado = None
            flujo = us.flujo
            if  proyecto_actual.flujo_set.filter(nombre=flujo.nombre).count() == 0:
                flujo2 = Flujo.objects.create(proyecto=proyecto_actual, nombre=flujo.nombre)
                flujo2.save()
                flujos = flujo.get_fases_flujo()
                for fase in flujos:
                    fase2 = fase
                    fase2.pk = None
                    fase2.save()
                    flujo2.faseaflujo.add(fase2)
            else:
                flujo2 = proyecto_actual.flujo_set.filter(nombre=flujo.nombre).first()

            us2.fase_actual = flujo2.faseaflujo.get(orden=1)
            us2.flujo = flujo2

            us2.save()

            sprint2.userStories.add(us2)
            sprint2.save()

        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context['pk'] = pkp
            return context

        # def get_success_url(self):
        #     """
        #     Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        #     :return: url de redireccionamiento
        #     """
        #     return reverse_lazy('Proyecto:sprint:impostarS', kwargs={'pk': self.kwargs['pk'],
        #                                                              'pks': self.kwargs['pks'],
        #                                                              'pkp': self.kwargs['pkp']})

        return HttpResponseRedirect(reverse_lazy('Proyecto:sprint:listaSprint', kwargs = {'pk': pkp}))


@permission_required_or_403('Sprint.iniciar_sprint', (Proyecto, 'id', 'pk'))
def iniciarSprintView(request, pk, pks):
    """
    Inicia el sprint seleccionado, setea las fechas iniciales y fechas finales estimadas, el estado y los estados y fases
    de los user stories del sprint backlog
    :param request: el request http
    :param pk: el primary key del proyecto
    :param pks: el primary key del sprint
    :return: retorna una redireccion htttp a la lista de sprints
    """
    if request.method == 'POST':
        sprint = Sprint.objects.get(pk=pks)
        proyecto = Proyecto.objects.get(pk=pk)
        sprint.fechaInicio = datetime.now().date()
        sprint.fechaFinalCalculada = calcularFechaFinal(sprint)
        sprint.fechaFinal = calcularFechaFinal(sprint, duracion=sprint.duracion)
        sprint.estado = 2
        sprint.save()

        for us in sprint.userStories.all():
            us.estado = 2
            us.estado_actual = 'To do'
            us.fase_actual = us.flujo.faseaflujo.get(orden=1)

            us.save()
        # return render(request, 'Sprint/sprint_list.html', context={'proyecto': proyecto, 'sprint_list': Sprint.objects.all().filter(proyecto_id=pk)})
        return HttpResponseRedirect(reverse_lazy('Proyecto:sprint:listaSprint', kwargs={'pk': pk }))



def reporte_US(request, pk, pks):
    sprint = get_object_or_404(Sprint, pk= pks)
    proyecto = get_object_or_404(Proyecto, pk= pk)
    setuserstories= sprint.userStories.all()

    context = {
        'user_stories_pendientes' : setuserstories.filter(estado=2, prioridad=10),
        'sprint' : sprint,
        'proyecto': proyecto
    }

    html_string = render_to_string('reportes/pdf_template_US.html', context)

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/US_pendientes_prioridad_maxima.pdf')

    from django.core.files.storage import FileSystemStorage
    fs = FileSystemStorage('/tmp')
    with fs.open('US_pendientes_prioridad_maxima.pdf') as pdf:
        from django.http import HttpResponse
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="US_pendientes_prioridad_maxima.pdf"'
        return response




def reporte_sprint_backlog(request, pk, pks):
    sprint = get_object_or_404(Sprint, pk=pks)
    proyecto= get_object_or_404(Proyecto, pk=pk)
    context = {
        'user_stories_inicial' : sprint.userStories.filter(estado=1),
        'user_stories_en_desarrollo' : sprint.userStories.filter(estado=2),
        'user_stories_terminados' : sprint.userStories.filter(estado=3),
        'user_stories_cancelados' : sprint.userStories.filter(estado=4),
        'proyecto': proyecto,
        'sprint':sprint
    }

    html_string = render_to_string('reportes/pdf_template_product_backlog.html', context)

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf')

    fs = FileSystemStorage('/tmp')
    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="mypdf.pdf"'
        return response
