from django import forms
from UserStory.models import UserStory
from .models import Sprint, SprintUserMember
from django.contrib.admin import widgets
from team.models import team
from Proyecto.models import Proyecto

class CrearSprintForm(forms.ModelForm):

    # fechaInicio = forms.DateField(
    #     label='Fecha de inicio',
    #     widget=forms.DateInput(
    #         attrs={'class': ' form-control datepicker', 'name': 'fechaInicio', 'data-date-format':'dd-mm-yyyy','data-date-viewmode':'years'}),
    #     help_text='Fecha de inicio del sprint')
    #
    # fechaFinal = forms.DateField(
    #     label='Fecha final',
    #     widget=forms.DateInput(
    #         attrs={'class': ' form-control datepicker', 'name': 'fechaFinal', 'data-date-format':'dd-mm-yyyy'}),
    #     help_text='Fecha final del sprint')

    userStories = forms.ModelMultipleChoiceField(
        queryset=UserStory.objects.filter(sprint=None),
        widget=widgets.FilteredSelectMultiple(verbose_name='User Stories', is_stacked=False),
        required=True
    )

    estado = forms.IntegerField(initial=1, disabled=True)

    proyecto = forms.ModelChoiceField(queryset=Proyecto.objects.all(), disabled=True, required=False)

    duracion = forms.IntegerField(required=True)

    # def clean(self):
    #     inicio = self.cleaned_data['fechaInicio']
    #     fin = self.cleaned_data['fechaFinal']
    #     # if inicio < datetime.now.:
    #     #     raise forms.ValidationError("Las fecha de inicio es invalida")
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")

    class Meta:
        model = Sprint
        fields = ['userStories','estado','duracion']


class UpdateSprintForm(forms.ModelForm):

    fechaInicio = forms.DateField(
        label='Fecha de inicio',
        widget=forms.DateInput(
            attrs={'class': ' form-control datepicker', 'name': 'fechaInicio', 'data-date-format':'dd-mm-yyyy','data-date-viewmode':'years'}),
        help_text='Fecha de inicio del sprint')

    fechaFinal = forms.DateField(
        label='Fecha final',
        widget=forms.DateInput(
            attrs={'class': ' form-control datepicker', 'name': 'fechaFinal', 'data-date-format':'dd-mm-yyyy'}),
        help_text='Fecha final del sprint')

    def clean(self):
        inicio = self.cleaned_data['fechaInicio']
        fin = self.cleaned_data['fechaFinal']
        # if inicio < datetime.now.:
        #     raise forms.ValidationError("Las fecha de inicio es invalida")
        if inicio >= fin:
            raise forms.ValidationError("Las fechas son invalidas")  # def clean(self):


    # def clean_fechaFinal(self):
    #     inicio = self.cleaned_data['fechaInicio']
    #     fin = self.cleaned_data['fechaFinal']
    #     if inicio >= fin:
    #         raise forms.ValidationError("Las fechas son invalidas")

    class Meta:
        model = Sprint
        fields = ['fechaInicio', 'fechaFinal']

class CrearSprintForm2(forms.ModelForm):

    class Meta:
        model = UserStory
        fields = ['encargado']



class ReasignarUS(forms.ModelForm):

    class Meta:
        Model = UserStory
        fields = ['encargado']

class ImportarSprint(forms.ModelForm):

    class Meta:
        model = Sprint
        fields = ['proyecto', 'userStories']


class Cancelar(forms.ModelForm):
    """Form para manejar la creación de las reuniones restrospectivas"""
    class Meta:
        model= Sprint
        fields =['reunion']