from celery.schedules import crontab
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from datetime import datetime

from django.template.loader import render_to_string

from Sprint.models import Sprint

logger = get_task_logger(__name__)
"""Definición de la tarea periodica"""
@periodic_task(
    #ejecutar la verificación cada día a las 6 de la mañana todos los dias
    run_every=(crontab(hour=6, minute=0)),
    name="sprint_superado",
    ignore_result=True
)
def verificar_tiempo_sprint():
    """
    Task celery que verifica todos los días que el sprint no haya superado su fecha de
        fin.
    """
    sprint= Sprint.objects.filter(estado=2)

    for s in sprint:
        if(s != None):

            # Si la fecha actual es mayor a la de finalización
            if(datetime.now().date() > s.fechaFinal):
                proyecto= s.proyecto
                for team in proyecto.team_set.all():
                    if (team.user.has_perm('Kanban.mover_kanban', proyecto)):
                        scrum= team.user
                        # Envia el correo de notificación al Scrum Master
                        mail_subject = 'Notificación : Tiempo de Sprint superado'
                        message_scrum = render_to_string('Sprint/notif_tiemposprint.html', {
                            'user': scrum,
                            'sprint': sprint,
                        })
                        team.user.email_user(mail_subject, message_scrum)
                        logger.info("Mail enviado a" + team.user.username + " por tiempo superado de sprint " + sprint.numero + "del proyecto " + sprint.proyecto.nombre)
                else:
                    logger.info("La fecha actual no es mayor que la de finalizacion del sprint")
    else:
        logger.info("No se encuentra ningun sprint en ejecución")





