from django.test import TestCase
from Accounts.models import User, Rol
from UserStory.models import UserStory
from Proyecto.models import Proyecto
from team.models import team
from guardian.shortcuts import assign_perm
from django.urls import reverse
from Sprint.models import Sprint, SprintUserMember
from Kanban.models import Flujo, Fase



# Create your tests here.

class SprintCreateViewTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_crear_sprint(self):
        """
        Se crean instancias de proyecto, team y rol. Se asigna permisos de crear sprint al usuario,
        se crea un sprint a traves de la vista con tres mensajes post, se verifica la redirección a la lista de sprint
        y el código de respuesta http.
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.crear_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)


        horas = 'horas_' + str(t1.id)
        dataStep1 = {'duracion': '5',
                     'userStories': str(userStory.id),
                     horas: 5,
                     str(userStory.id): str(t1.id)}

        response = self.client.post(reverse('Proyecto:sprint:crearSprint', kwargs={'pk': str(proyecto.id)}),
                                    data=dataStep1, follow=True)





        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('Proyecto:sprint:listaSprint', kwargs={'pk': proyecto.id}))

    def test_crear_sprint_sin_pemriso(self):
        """
        Se crean instancias de proyecto, team y rol. No se asigna permisos de crear sprint al usuario,
        se accede a la pagina de creacion de sprint y se espera una respuesta http 403
        """
        usuario = self.setup_user()

        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        rol1 = Rol.objects.create(isProyectoRol=True)
        t1 = team.objects.create(user=usuario, proyecto=proyecto, rol=rol1)

        dataStep1 = {'crear__sprint-current_step': '0', '0-duracion': '5',
                  '0-userStories': str(userStory.id)}

        response = self.client.post(reverse('Proyecto:sprint:crearSprint', kwargs={'pk': str(proyecto.id)}),
                                    data=dataStep1,
                                    follow=True)

        self.assertEqual(response.status_code, 403)

        response = self.client.get(reverse('Proyecto:sprint:crearSprint', kwargs={'pk': str(proyecto.id)}), follow=True)

        self.assertEqual(response.status_code, 403)


    def test_iniciar_sprint(self):
        """
        Se crean instancias de sprint y se agregan user stories, se comprueba con un mensaje post que el sprint haya sido
        puesto en estado iniciado
        :return:
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.iniciar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)



        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        t1 = team.objects.create(user=usuario, proyecto=proyecto, rol=rol1, horas_trabajadas=5)

        sprint = Sprint.objects.create(duracion=4, proyecto=proyecto)

        flujo = Flujo.objects.create(proyecto=proyecto)
        fase = Fase.objects.create(orden=1, flujo=flujo)

        userStory = UserStory.objects.create(proyecto=proyecto, estado=1, tiempo_estimado_de_terminacion=20, flujo=flujo)
        sprint.userStories.add(userStory)



        response = self.client.post(reverse('Proyecto:sprint:iniciarSprint', kwargs={'pk': str(proyecto.id),
                                                                                     'pks': str(sprint.id)}), follow=True)



        sprint = Sprint.objects.get(pk=sprint.id)
        userStory = UserStory.objects.get(pk=userStory.id)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(sprint.get_estado_display(), 'En Ejecución')
        self.assertEqual(userStory.estado_actual, 'To do')





class ModificarSprintTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_menu_modificar_sprint(self):
        """
        Se crean instancias de proyecto, team y rol. Se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se accede al menu modificar con un mensajes get, se verifica el template usado
        y el código de respuesta http.
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.modificar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:modificarSprint',
                                           kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}), follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_us_listar.html')

    def test_menu_modificar_sprint_sin_permiso(self):
        """
        Se crean instancias de proyecto, team y rol. No se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se accede al menu modificar con un mensajes get, se verifica que el codigo de respuesta
        http sea 403
        :return:
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True, name='scrum')
        assign_perm('Sprint.modificar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)

        rol2 = Rol.objects.create(isProyectoRol=True, name='developer')

        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        proyecto2 = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')

        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol2, proyecto=proyecto)
        t2 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto2)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:modificarSprint',
                                           kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}), follow=True)

        self.assertEqual(response.status_code, 403)

    def test_reasignar_us(self):
        """
        Se crean instancias de proyecto, team y rol. Se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se intenta modificar el usuario asignado de un user con un mensaje post,
        se verifica el template usado, el código de respuesta http y la redireccion a la lista de user stories.
        """

        usuario = self.setup_user()

        usuario2 = User(username='jorge')
        usuario2.set_password('123456')
        usuario2.email = 'email2@gmail.com'
        usuario2.save()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.modificar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        t2 = team.objects.create(user=usuario2, rol=rol1, proyecto=proyecto)

        response = self.client.post(reverse('Proyecto:sprint:reasignarUS',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id),
                                                    'pku': str(userStory.id)}),
                                    follow=True, data={'encargado': str(t2.id)})

        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('Proyecto:sprint:modificarSprint',
                                                 kwargs={'pk': proyecto.id, 'pks': sprint.id}))
        self.assertTemplateUsed(response, 'Sprint/sprint_us_listar.html')

    def test_reasignar_us_sin_permiso(self):
        """
        Se crean instancias de proyecto, team y rol. No se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se intenta modificar el usuario asignado de un user con un mensaje post,
        se verifica el codigo de respuesta http sea 403.
        """

        usuario = self.setup_user()

        usuario2 = User(username='jorge')
        usuario2.set_password('123456')
        usuario2.email = 'email2@gmail.com'
        usuario2.save()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        t2 = team.objects.create(user=usuario2, rol=rol1, proyecto=proyecto)

        response = self.client.post(reverse('Proyecto:sprint:reasignarUS',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id),
                                                    'pku': str(userStory.id)}),
                                    follow=True, data={'encargado': str(t2.id)})

        self.assertEqual(response.status_code, 403)

    # def test_remover_us(self):
    #     """
    #     Se crean instancias de proyecto, team, rol y user story. Se asigna permisos de modificar sprint al usuario,
    #     se crea un sprint y se intenta remover un user story del sprint backlog con un mensaje post,
    #     se verifica el codigo de respuesta http sea 200 y el template usado es el que corresponde.
    #     """
    #     usuario = self.setup_user()
    #
    #     rol1 = Rol.objects.create(isProyectoRol=True)
    #     assign_perm('Sprint.modificar_sprint', rol1)
    #     assign_perm('Sprint.listar_sprint', rol1)
    #     proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
    #     userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
    #     userStory.tiempo_estimado_de_terminacion= 120
    #     t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
    #     sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)
    #     sprint.userStories.add(userStory)
    #
    #     sprintusermember = SprintUserMember.objects.create(horasDedicadas=8, sprint=sprint, member=t1)
    #
    #     response = self.client.post(reverse('Proyecto:sprint:removerUS',
    #                                         kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id),
    #                                                 'pku': str(userStory.id)}),
    #                                 follow=True, )
    #
    #     self.assertEqual(response.status_code, 200)
    #     self.assertTemplateUsed(response, 'Sprint/sprint_us_listar.html')

    def test_remover_us_sin_permiso(self):
        """
        Se crean instancias de proyecto, team, rol y user story. No se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se intenta remover un user story del sprint backlog con un mensaje post,
        se verifica el codigo de respuesta http sea 403
        """

        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)
        sprint.userStories.add(userStory)

        response = self.client.post(reverse('Proyecto:sprint:removerUS',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id),
                                                    'pku': str(userStory.id)}),
                                    follow=True, )

        self.assertEqual(response.status_code, 403)

    def test_agregar_nuevo_us(self):
        """
        Se crean instancias de proyecto, team, rol y user story. Se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se intenta agregar un nuevo user story a traves de la vista con un mensaje post,
        se verifica el codigo de respuesta http sea 200 y el template usado es el que corresponde.
        """
        usuario = self.setup_user()

        usuario2 = User(username='jorge')
        usuario2.set_password('123456')
        usuario2.email = 'email2@gmail.com'
        usuario2.save()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.modificar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')

        flujo = Flujo.objects.create(proyecto=proyecto)
        fase = Fase.objects.create(orden=1, flujo = flujo)

        userStory = UserStory.objects.create(proyecto=proyecto, estado=1, flujo=flujo)
        userStory.tiempo_estimado_de_terminacion = 30
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)
        sprint.userStories.add(userStory)
        sprintusermember = SprintUserMember.objects.create(horasDedicadas=8, sprint=sprint, member=t1)

        userStory2 = UserStory.objects.create(proyecto=proyecto, estado=1, flujo=flujo)
        userStory2.tiempo_estimado_de_terminacion = 120
        t2 = team.objects.create(user=usuario2, rol=rol1, proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:agregarUS',
                                           kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}),
                                   follow=True, )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_nuevo_us.html')

        response = self.client.post(reverse('Proyecto:sprint:agregarUS',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}),
                                    data={'encargado': t1.id, 'userstory': userStory2.id},
                                    follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_us_listar.html')

    def test_agregar_nuevo_us_sin_permiso(self):
        """
        Se crean instancias de proyecto, team, rol y user story. No se asigna permisos de modificar sprint al usuario,
        se crea un sprint y se intenta agregar un nuevo user story a traves de la vista con un mensaje post,
        se verifica el codigo de respuesta http sea 403.
        """
        usuario = self.setup_user()

        usuario2 = User(username='jorge')
        usuario2.set_password('123456')
        usuario2.email = 'email2@gmail.com'
        usuario2.save()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)
        sprint.userStories.add(userStory)

        userStory2 = UserStory.objects.create(proyecto=proyecto, estado=1)

        t2 = team.objects.create(user=usuario2, rol=rol1, proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:agregarUS',
                                           kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}),
                                   follow=True, )

        self.assertEqual(response.status_code, 403)

        response = self.client.post(reverse('Proyecto:sprint:agregarUS',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}),
                                    data={'encargado': t1.id, 'userstory': userStory2.id},
                                    follow=True)

        self.assertEqual(response.status_code, 403)


class ListaSprintTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_lista_sprints(self):
        """
        Se crean instancias de proyecto, team, rol y user story y sprint. Se asigna el permiso de
        listar sprints al usuario y a traves de la vista con un mensaje get, se accede a la pagina correspondiente
        se verifica el codigo de respuesta http sea 200 y el template mostrado es el correcto.
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True, name='scrum')
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:listaSprint',
                                           kwargs={'pk': str(proyecto.id)}), follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_list.html')

    def test_lista_sprints_sin_permiso(self):
        """
        Se crean instancias de proyecto, team, rol y user story y sprint. No se asigna el permiso de
        listar sprints al usuario y a traves de la vista con un mensaje get, se intenta acceder a la pagina correspondiente
        se verifica el codigo de respuesta http sea 403.
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True, name='scrum')
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.get(reverse('Proyecto:sprint:listaSprint',
                                           kwargs={'pk': str(proyecto.id)}), follow=True)

        self.assertEqual(response.status_code, 403)


class EliminarSprintTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_eliminar_sprint(self):
        """
        Se crean instancias de proyecto, team, rol y user story y sprint. Se asigna el permiso de
        eliminar sprints al usuario y a traves de la vista con un mensaje post, se intenta eliminar un sprint.
        Se verifica el codigo de respuesta sea 200 y el template mostrado es el de listar user stories
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.eliminar_sprint', rol1)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.post(reverse('Proyecto:sprint:eliminarSprint',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}), follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_list.html')

    def test_eliminar_sprint_sin_permiso(self):
        """
        Se crean instancias de proyecto, team, rol y user story y sprint. No se asigna el permiso de
        eliminar sprints al usuario y a traves de la vista con un mensaje post, se intenta eliminar un sprint.
        Se verifica el codigo de respuesta sea 403
        """
        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto)

        response = self.client.post(reverse('Proyecto:sprint:eliminarSprint',
                                            kwargs={'pk': str(proyecto.id), 'pks': str(sprint.id)}), follow=True)

        self.assertEqual(response.status_code, 403)


class ImportarSprintTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_importar_sprint(self):
        usuario = self.setup_user()

        rol2 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.importar_sprint', rol2)
        assign_perm('Sprint.listar_sprint', rol2)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol2, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto, estado=1)

        response = self.client.post(reverse('Proyecto:sprint:importarS',
                                            kwargs={'pk': str(sprint.proyecto.id), 'pks': str(sprint.id), 'pkp': str(proyecto.id)}), follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Sprint/sprint_list.html')

    def test_importar_sprint_sin_permiso(self):

        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Sprint.listar_sprint', rol1)
        proyecto = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto, estado=1)

        response = self.client.post(reverse('Proyecto:sprint:importarS',
                                            kwargs={'pk': str(sprint.proyecto.id), 'pks': str(sprint.id), 'pkp': str(proyecto.id)}), follow=True)

        self.assertEqual(response.status_code, 403)
