from django.urls import path
from . import views
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.contrib.auth.decorators import login_required
from guardian.decorators import permission_required_or_403


# Urls del sistema
app_name = 'Accounts'
urlpatterns = [
    path('', views.home, name='home'),
    path('roles/', views.ListaRoles.as_view(), name='listaRol'),
    path('crear/', views.CrearRol.as_view(), name='crearRol'),
    path('crearPr/', views.CrearRolPr.as_view(), name='crearRolPr'),
    path('<int:pk>/modificar/', views.ModificarRoles.as_view(), name='modificarRol'),
    path('<int:pk>/modificarPr/', views.ModificarRolesProyecto.as_view(), name='modificarRolPr'),
    path('<int:pk>/eliminar/', views.EliminarRol.as_view(), name='eliminarRol'),
    path('<int:pk>/asignar_rol', views.asignarRol.as_view(), name='asignarRol'),
    # path('<int:pk>/eliminar/', views.EliminarRol.as_view(), name='eliminarRol'),
    path('proyectos/list', views.pro_list, name='pro_list'),
    path('usuarios/crear', views.user_mincrear.as_view(), name='user_createmin'),
    path('usuarios/listar', views.ListaUsuarios.as_view(), name='user_listar'),
    path('usuarios/<int:pk>/modificar/', views.ModUser.as_view(), name='user_modificar'),
    path('usuarios/<int:pk>/eliminar/', views.DelUsuarios.as_view(), name='user_eliminar'),
    path('usuarios/<int:pk>/cambiarPass/', views.CambiarPassView.as_view(), name='password_change'),
    path('resetPass/', views.ResetPassView.as_view(), name='password_reset'),
    path('resetPassDone/', PasswordResetDoneView.as_view(template_name='password_reset_done.html'), name='my_password_reset_done'),
    path('reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(template_name='password_reset_confirm.html', success_url=reverse_lazy('Accounts:password_reset_complete')), name='password_reset_confirm'),
    path('reset/done/', PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'), name='password_reset_complete')
]
