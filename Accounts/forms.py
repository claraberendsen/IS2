from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.forms import AuthenticationForm
from django import forms
from django.contrib.auth.models import Group, Permission
from  Accounts.models import  User, Rol


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30,
                               widget=forms.TextInput(attrs={'class': 'validate[required] form-control', 'name': 'username', 'placeholder':'username'}))
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.PasswordInput(attrs={'class': 'validate[required,minSize[8]] form-control', 'name': 'password', 'placeholder':'password'}))

class ModUserForm (forms.ModelForm):
    username = forms.CharField(label='username', max_length=10,
                               widget=forms.TextInput(attrs={'class':'form-control', 'name':'username', }))
    email = forms.CharField(label='email address',
                            widget=forms.TextInput(attrs={'class':' form-control', 'name':'email'}))
    first_name = forms.CharField(label='first_name',max_length=80,
                        widget = forms.TextInput(attrs={'class': ' form-control', 'name': 'first_name'}))
    last_name = forms.CharField(label='last_name',max_length=80,
                        widget = forms.TextInput(attrs={'class': ' form-control', 'name': 'last_name'}))

    phone = forms.CharField(label='phone', max_length=25,
                        widget = forms.TextInput(attrs={'class':'form-control', 'name': 'phone'}))

    address = forms.CharField(label='address',max_length=80,
                        widget = forms.TextInput(attrs={'class': ' form-control', 'name': 'address'}))

    gender = forms.CharField(label='gender',max_length=1,
                        widget = forms.TextInput(attrs={'class': ' form-control', 'name': 'gender'}))

    date_birth = forms.DateTimeField(label='date_birth',
                        widget = forms.DateTimeInput(attrs={'class':' form-control', 'name': 'date_birth'}))

    groups = forms.ModelMultipleChoiceField(queryset=Rol.objects.all().exclude(isProyectoRol=True),
                                            widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = User
        fields= ('username', 'email', 'first_name', 'last_name', 'phone','address', 'gender', 'date_birth','groups')


class CreateUserMinForm (forms.ModelForm):
    username = forms.CharField(label='username', max_length=10,
                               widget=forms.TextInput(attrs={'class':'form-control', 'name':'username', }))
    email = forms.EmailField(label='email address',
                            widget=forms.TextInput(attrs={'class':' form-control', 'name':'email'}))

    groups = forms.ModelMultipleChoiceField(queryset=Rol.objects.all().exclude(isProyectoRol=True), widget=forms.CheckboxSelectMultiple())
    class Meta:
        model = User
        fields= ('username', 'email', 'groups')

class RolForm(forms.ModelForm):
    """
    Form para la creacion de un rol, especificando el widget como CheckboxSelectMultiple
    """
    permissions = forms.ModelMultipleChoiceField(queryset=Permission.objects.filter(content_type_id__in=(6,7,8,9)),widget=forms.CheckboxSelectMultiple(), required=False)

    class Meta:
        model = Rol
        exclude = ['user','isProyectoRol']

class asignarRolForm(forms.ModelForm):

    groups = forms.ModelMultipleChoiceField(queryset=Group.objects.all(),widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = User
        fields = ['groups']


class RolProyectoFrom(forms.ModelForm):

    permissions = forms.ModelMultipleChoiceField(
        queryset=Permission.objects.filter(content_type__in=(10,11,12,14,13,15,16)), widget=forms.CheckboxSelectMultiple())

    isProyectoRol = forms.BooleanField(initial=True, disabled=True, label='Rol de Proyecto')
    class Meta:
        model = Rol

        exclude = ['user']

