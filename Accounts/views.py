from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect, HttpResponseRedirect, render_to_response
from django.template.loader import render_to_string
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth import views

from Accounts.forms import CreateUserMinForm, ModUserForm
from Accounts.forms import RolForm, asignarRolForm, RolProyectoFrom
from Accounts.models import User, Rol
from django.db import IntegrityError


def home(request):
    """
    :param request:

    :return HttpResponse("Bienvenido"):
    """
    return render(request, "index.html")


def pro_list(request):
    """
    :param request:

    :return HttpResponse("Bienvenido"):
    """
    return render(request, "pro/pro_listar.html")


class user_mincrear(CreateView):
    model=User
    template_name = 'user/user_createmin.html'
    form_class = CreateUserMinForm
    success_url = reverse_lazy('Accounts:user_listar')

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        user = form.save(commit=False)
        password = BaseUserManager().make_random_password(15)
        user.set_password(password)
        user.is_staff = True
        user.save()
        mail_subject = 'Usuario para Gestpro'
        message = render_to_string('user/email_pass.html', {
            'user': user,
            'password':password
        })
        user.email_user(mail_subject,message)
        return super().form_valid(form)


class ModUser(UpdateView):
    model = User
    template_name = 'user/user_mod.html'
    form_class = ModUserForm
    success_url = reverse_lazy('Accounts:user_listar')



class CrearRol(PermissionRequiredMixin,CreateView):
    """
    View generico para la creacion de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y CreateView para crear nuevos roles
    """
    model = Rol
    template_name = 'rol/rol_create.html'
    form_class=RolForm
    success_url = reverse_lazy('Accounts:listaRol')

    permission_required = 'Accounts.crear_rol'
    raise_exception = True

    def post(self, request, *args, **kwargs):
        return super(CrearRol, self).post(request, *args, **kwargs)


class CrearRolPr(PermissionRequiredMixin,CreateView):
    """
    View generico para la creacion de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y CreateView para crear nuevos roles
    """
    model = Rol
    template_name = 'rol/rol_create.html'
    form_class=RolProyectoFrom
    success_url = reverse_lazy('Accounts:listaRol')


    permission_required = 'Accounts.crear_rol'
    raise_exception = True



class ListaUsuarios(ListView):
    model = User
    context_object_name = 'my_users'
    template_name = 'user/user_listar.html'



class DelUsuarios(DeleteView):
    model = User
    success_url = reverse_lazy('Accounts:user_listar')
    permission_required = 'User.delete_user'
    raise_exception = True

class ListaRoles(PermissionRequiredMixin,generic.ListView):
    """
    View genérica para la creacion de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y ListView para ver la lista de Roles
    """
    model = Rol
    template_name = 'rol/rol_listar.html'
    context_object_name = 'my_roles'
    permission_required = 'Accounts.ver_rol'
    raise_exception = True

    def get_context_data(self, *args, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context["message"]=self.request.GET["msg"]
        except :
            context["message"]=None
        return context

class ModificarRoles(PermissionRequiredMixin, generic.UpdateView):
    """
    View genérica para la modificación de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y UpdateView para modificar los atributos de un rol
    """
    model = Rol
    form_class = RolForm
    template_name = 'rol/rol_mod.html'
    permission_required = 'Accounts.modificar_rol'
    success_url = reverse_lazy('Accounts:listaRol')
    raise_exception = True

    def post(self, request, *args, **kwargs):
        """
        Override del metodo post, para controlar el caso en que se eliga la opción de eliminar el rol y redirigir al
        view correspondiente, en otro caso se continúa llamando al método de la clase padre.
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        if 'delete' in request.POST:
            return redirect('Accounts:eliminarRol', **kwargs)
        return super().post(request, *args, **kwargs)

class ModificarRolesProyecto(PermissionRequiredMixin, generic.UpdateView):
    """
    View genérica para la modificación de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y UpdateView para modificar los atributos de un rol
    """
    model = Rol
    form_class = RolProyectoFrom
    template_name = 'rol/rol_mod.html'
    permission_required = 'Accounts.modificar_rol'
    success_url = reverse_lazy('Accounts:listaRol')
    raise_exception = True

class EliminarRol(PermissionRequiredMixin, generic.DeleteView):
    """
    View genérica para la modificación de roles, hereda de PermissionRequiredMixin para implementar los permisos de acceso
    a las urls y DeleteView para eliminar Roles
    """
    model = Rol
    success_url = reverse_lazy('Accounts:listaRol')
    template_name = 'Accounts/group_confirm_delete.html'

    permission_required = 'Accounts.eliminar_rol'
    raise_exception = True

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        try:
            self.object.delete()
        except IntegrityError as e:
            #return render(request, 'rol/rol_listar.html', {"message": 'Algun usuario sigue asignado a ese rol'})
            msg= {"message": 'Algun usuario sigue asignado a ese rol'}
            #kwargs["message"] = 'Algun usuario sigue asignado a ese rol'
            return redirect('/roles/?msg=Algun usuario sigue asignado a ese rol')

        return HttpResponseRedirect(success_url)





# def asignarRol(request, user_id):
#     roles = Group.objects.all()
#     if request.method == 'POST':
#         roles_seleccionados = request.POST.getlist('rol')
#         user = User.objects.get(pk=user_id)
#         for rol in roles_seleccionados:
#             user.groups.add(rol)
#
#
#     return render(request, 'Accounts/asignar_rol.html', {'roles':roles})


class asignarRol(UpdateView):
    model = User
    template_name = 'Accounts/asignar_rol.html'
    form_class = asignarRolForm
    success_url = reverse_lazy('home')
    #
    # def form_valid(self, form):
    #     # resp = super(asignarRol, self).form_valid(form)
    #     # print(form.data.)
    #
    #     return HttpResponseRedirect(self.get_success_url())


class CambiarPassView(views.PasswordChangeView):
    template_name = 'password_change_form.html'

class ResetPassView(views.PasswordResetView):
    template_name = 'password_reset_form.html'
    success_url = reverse_lazy('Accounts:my_password_reset_done')
    email_template_name = 'password_reset_email.html'

