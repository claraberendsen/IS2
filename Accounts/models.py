from auditlog.registry import auditlog
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractUser, Permission, Group, ContentType
from django.db import models

# Clase modelo para el usuario



class User(AbstractUser):
    """
    Custom user class
    """
    username = models.CharField('username', max_length=50, unique=True,
                                db_index=True)
    email = models.EmailField('email address', unique=True)
    first_name= models.CharField(max_length=80)
    last_name= models.CharField(max_length=80)
    is_active = models.BooleanField(default=True)
    joined = models.DateTimeField(auto_now_add=True)
    is_admin = models.BooleanField(default=False)
    phone = models.CharField(max_length=25)
    address = models.CharField(max_length=80)
    gender = models.CharField(max_length=1)
    date_birth = models.DateField(blank=True, null=True)
    USERNAME_FIELD = 'username'
    groups = models.ManyToManyField(
        Group,
        verbose_name=('groups'),
        blank=True,
        help_text=(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name="user_set",
        related_query_name="user",
    )

    class Meta:
        default_permissions = []
        permissions = (
            ("modificar_usuario", "Modificar usuarios"),
            ("crear_usuario", "Crear usuarios"),
            ("eliminar_usuario", "Eliminar usuarios")
        )



    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __unicode__(self):
        return self.username


def getPermisos(self):
    return self.name

Permission.add_to_class("__str__",getPermisos)


class Rol(Group):
    isProyectoRol = models.BooleanField(default=False)


    class Meta:
        default_permissions = []
        permissions = (
            ("modificar_rol", "Modificar roles"),
            ("crear_rol", "Crear roles"),
            ("eliminar_rol", "Eliminar roles"),
            ("ver_rol", "Ver lista de roles"),
        )

# try:
#     Permission.objects.get(codename='ver_roles')
# except Permission.DoesNotExist:
#     content_type = ContentType.objects.get_for_model(Group)
#     permission = Permission.objects.create(codename='ver_roles', name='Puede acceder a lista de roles',
#                                        content_type=content_type,)


auditlog.register(User)

