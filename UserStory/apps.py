from django.apps import AppConfig


class UserstoryConfig(AppConfig):
    name = 'UserStory'
