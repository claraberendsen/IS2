from django.urls import path, include
from .views import  Crear_User_Story, Lista_Us, Modificar_User_Story, Eliminar_User_Story, Redefinir_User_Story,\
    Lista_Us_History, cancelarus



# Urls del sistema
app_name = 'UserStory'
urlpatterns = [
    path('', Lista_Us.as_view(), name='listaUs'),
    path('crear/', Crear_User_Story.as_view(), name='crearUs'),
    path('<int:pku>/modificar/', Modificar_User_Story.as_view(), name='modificarUs'),
    path('<int:pku>/eliminar/', Eliminar_User_Story.as_view(), name='eliminarUs'),
    # path('<int:pku>/redefinir/', Redefinir_User_Story.as_view(), name='redefinirUs'),
    path('<int:pku>/historial/', Lista_Us_History.as_view(), name='historialUs'),
    path('<int:pku>/cancelar/', cancelarus, name='cancelarUs'),
    path('<int:pku>/UserStory/Dailies/', include('Daily.urls'))
    ]
