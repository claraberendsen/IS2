from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.mixins import PermissionRequiredMixin
from guardian import mixins
from Kanban.models import Flujo, Fase

# Login view

from django.views.generic.edit import CreateView, UpdateView

# Create your views here.
from guardian.decorators import permission_required_or_403

from .models import UserStory, UserStoryHistory
from .forms import UserStoryForm
from Proyecto.models import Proyecto

class Crear_User_Story(mixins.PermissionRequiredMixin, CreateView):

    model = UserStory
    template_name = 'us/us_crear.html'
    form_class = UserStoryForm
    permission_required = 'UserStory.crear_us'
    raise_exception = True

    def get_form(self, form_class=None):
        form = super(Crear_User_Story, self).get_form(form_class)
        form.fields['flujo'].queryset = Flujo.objects.filter(proyecto=self.kwargs['pk'])
        return form

    def get_permission_object(self):
        """
         get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_form(self, form_class=None):
        """
        Se actualiza el queryset del form
        :param form_class:
        :return:
        """
        form = super().get_form(form_class)
        form.fields['flujo'].queryset = Flujo.objects.filter(proyecto=self.kwargs['pk'])
        return form

    def form_valid(self, form):
        """
        form_valid permite alterar el form generico por defecto que acompanha a CreateView
        :param form, self: el form generico
        :return : el form generico modificado por form_valid
        """
        proyec = self.kwargs['pk']
        form.instance.proyecto = Proyecto.objects.filter(id=proyec).first()
        form.instance.estado = 1
        if form.instance.urgencia == None :
            form.instance.urgencia =0
        if form.instance.valor_de_negocio == None:
            form.instance.valor_de_negocio = 0
        if form.instance.valor_tecnico == None:
            form.instance.valor_tecnico = 0
        form.instance.prioridad = (form.instance.urgencia + form.instance.valor_de_negocio + form.instance.valor_tecnico)/3
        return super(Crear_User_Story, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:listaUs',
                            kwargs={'pk': self.kwargs['pk']})

class Lista_Us(generic.ListView):
    """
    Lista_US hereda de de la vista generica ListView y PermissionRequiredMixin
    Requiere que el usuario posea el  permiso listar_us para poder ser utilizada
    """
    model = UserStoryForm
    template_name = 'us/us_listar.html'
    context_object_name = 'my_userstorys'

    raise_exception = True

    def get(self, request, *args, **kwargs):
        if (not request.user.is_superuser) and request.user.team_set.filter(proyecto__id=kwargs['pk']).count() == 0:
            raise Http404('No sos parte de este proyecto')
        return super(Lista_Us, self).get(request,*args,**kwargs)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        try:
            context["message"] = self.request.GET["msg"]
        except:
            context["message"] = None
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Una lista de objetos UserStory
        """
        return UserStory.objects.filter(proyecto=self.kwargs['pk'])


class Lista_Us_History(mixins.PermissionRequiredMixin, generic.ListView):
    """
    Lista_Us_History hereda de de la vista generica ListView y PermissionRequiredMixin
    Requiere que el usuario possea el  permiso listar_us_history para poder ser utilizada
    """
    model = UserStoryForm
    template_name = 'us/us_historial.html'
    context_object_name = 'my_userstorys'

    permission_required = 'UserStory.listar_us_history'
    raise_exception = True


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])

        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Una lista de objetos UserStory
        """
        return UserStoryHistory.objects.filter(id_us=self.kwargs['pku'])


class Modificar_User_Story(mixins.PermissionRequiredMixin, UpdateView):
    """
       Modificar_User_Story hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el usuario possea el  permiso modificar_us para poder ser utilizada
    """
    model = UserStory
    pk_url_kwarg = 'pku'
    template_name = 'us/us_modificar.html'
    form_class = UserStoryForm
    permission_required = 'UserStory.modificar_us'
    raise_exception = True


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Una lista de objetos UserStory
        """

        return UserStory.objects.filter(id=self.kwargs['pku'])

    def get_form(self, form_class=None):
        """
        Se actualiza el queryset del form
        :param form_class:
        :return:
        """
        form = super().get_form(form_class)
        form.fields['flujo'].queryset = Flujo.objects.filter(proyecto=self.kwargs['pk'])
        return form

    def form_valid(self, form):
        """
        form_valid permite alterar el form generico por defecto que acompanha a CreateView
        :param form, self: el form generico
        :return : el form generico modificado por form_valid
        """

        proyec = self.kwargs['pk']
        form.instance.proyecto = Proyecto.objects.filter(id=proyec).first()
        form.instance.prioridad = (form.instance.urgencia + form.instance.valor_de_negocio + form.instance.valor_tecnico) / 3
        return super(Modificar_User_Story, self).form_valid(form)

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:listaUs',
                            kwargs={'pk': self.kwargs['pk']})

    def post(self, request, *args, **kwargs):
        """
        Se define la funcion post para detectar cuando se intenta eliminar un elemento y  redireccionar el pedido a
        la url Proyecto:Team:eliminarTeam,  y  se crea un objeto UserStoryvHistory del objeto inicial con toda su
        informacion  para ser almanacenado dentro del historial
        :param request:
        :param args:
        :param kwargs: Parametros necesarios para la view EliminarTeam
        :return: La funcion post de la clase parent
        """
        us = UserStory.objects.filter(id=self.kwargs['pku']).first()
        fase = None
        if us.fase_actual != None:
            fase = us.fase_actual.id

        UserStoryHistory.objects.create(id_us=us.id, proyecto=us.id, titulo=us.titulo,
                                        descripcion_corta=us.descripcion_corta, descripcion_larga=us.descripcion_larga,
                                        urgencia=us.urgencia, valor_tecnico=us.valor_tecnico,
                                        valor_de_negocio=us.valor_de_negocio, prioridad=us.prioridad, flujo=us.flujo,
                                        comentario_extra=us.comentario_extra, autor=request.user.username,
                                        horas_trabajadas=us.horas_trabajadas, estado=us.estado, archivo=us.archivo,
                                        fase_actual=fase, estado_actual=us.estado_actual)
        # Call the base implementation first to get a context
        if 'delete' in request.POST:
            return redirect('Proyecto:UserStory:eliminarUs', **kwargs)
        return super().post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        pkp = self.kwargs['pk']

        u = UserStory.objects.get(pk=self.kwargs['pku'])
        if u.sprint_set.count() > 0:
            return redirect(reverse_lazy('Proyecto:UserStory:listaUs',
                                         kwargs={
                                             'pk': self.kwargs['pk']}) + '?msg=El user story se encuentra en un sprint')

        if UserStory.objects.get(id=self.kwargs['pku']).proyecto.id == pkp:
            return super().get(self, request, *args, **kwargs)
        else:
            raise Http404("Hacker afuera!")

class Eliminar_User_Story(mixins.PermissionRequiredMixin, generic.DeleteView):
    """
    Eliminar_User_Story hereda de de la vista generica DeleteView y PermissionRequiredMixin
    Requiere que el usuario posea el  permiso eliminar_us para poder ser utilizada
    """
    model = UserStory
    pk_url_kwarg = 'pku'
    template_name = 'us/us_listar.html'
    permission_required = 'UserStory.eliminar_us'
    raise_exception = True



    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:listaUs',
                            kwargs={'pk': self.kwargs['pk']})


class Redefinir_User_Story( mixins.PermissionRequiredMixin, UpdateView):
    """
       Redefinir_User_Story hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el usuario posea el  permiso redefinir_us para poder ser utilizada
    """
    model = UserStory
    pk_url_kwarg = 'pku'
    template_name = 'us/us_modificar.html'
    form_class = UserStoryForm
    permission_required = 'UserStory.redefinir_us'
    raise_exception = True


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['redefinir'] = True
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Un objeto team
        """
        return UserStory.objects.filter(id=self.kwargs['pku'])

    def get_form(self, form_class=None):
        """
        Se actualiza el queryset del form
        :param form_class:
        :return:
        """
        form = super().get_form(form_class)
        form.fields['urgencia'].required = False
        form.fields['valor_de_negocio'].required = False
        form.fields['valor_tecnico'].required = False
        return form


    def form_valid(self, form):
        """
        form_valid permite alterar el form generico por defecto que acompanha a CreateView
        :param form, self: el form generico
        :return : el form generico modificado por form_valid
        """
        UserStory.objects.filter(id=self.kwargs['pku']).first().estado = 1
        proyec = self.kwargs['pk']
        form.instance.proyecto = Proyecto.objects.filter(id=proyec).first()
        #form.instance.estado = 1
        form.instance.urgencia=10
        form.instance.valor_de_negocio=10
        form.instance.valor_tecnico=10
        form.instance.prioridad = ( form.instance.urgencia + form.instance.valor_de_negocio + form.instance.valor_tecnico) / 3
        form.instance.sprint_set.clear()
        return super(Redefinir_User_Story, self).form_valid(form)

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:sprint:terminarSprint',
                             kwargs={'pk': self.kwargs['pk'], 'pks':self.kwargs['pks']})



    def post(self, request, *args, **kwargs):
        """
        Se define la funcion post para detectar cuando se redefine un UserStory, y crea un objeto UserStory
        History del objeto inicial con toda su informacion  para ser almanacenado dentro del historial
        :param request:
        :param args:
        :param kwargs: Parametros necesarios para la view EliminarTeam
        :return: La funcion post de la clase parent
        """
        us = UserStory.objects.filter(id=self.kwargs['pku']).first()

        fase = None
        if us.fase_actual != None:
            fase = us.fase_actual.id

        UserStoryHistory.objects.create(id_us=us.id, proyecto=us.id, titulo=us.titulo,
                                        descripcion_corta=us.descripcion_corta, descripcion_larga=us.descripcion_larga,
                                        urgencia=us.urgencia, valor_tecnico=us.valor_tecnico,
                                        valor_de_negocio=us.valor_de_negocio, prioridad=us.prioridad, flujo=us.flujo,
                                        comentario_extra=us.comentario_extra, autor=request.user.username,
                                        horas_trabajadas=us.horas_trabajadas, estado=us.estado, archivo=us.archivo,
                                        fase_actual=fase, estado_actual=us.estado_actual)

        return super().post(request, *args, **kwargs)


@permission_required_or_403('UserStory.eliminar_us', (Proyecto, 'id', 'pk'))
def cancelarus(request, pk, pku):
    """
    us.fase_actual.orden == Fase.objects.filter(flujo=us.fase_actual.flujo).count():
    """

    us = UserStory.objects.get(pk=pku)
    if us.estado == 1:
        us.estado = 4
        us.save()
    return HttpResponseRedirect(reverse_lazy('Proyecto:UserStory:listaUs', kwargs={'pk': pk}))

