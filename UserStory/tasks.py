from celery.schedules import crontab
from celery.task import periodic_task
from celery.task import  task
from celery.utils.log import get_task_logger
from django.template.loader import render_to_string

from Accounts.models import User
from UserStory.models import UserStory

logger = get_task_logger(__name__)


@task(name='mail_us_superado')
def mail_us_superado_task(user_pk, us_pk, scrum_pk):

    """Task Celery que envia un mail cuando el us supero su cantidad de horas planificadas de
        trabajo
        :param userpk Pk del usuario al cual esta asignado el user story
        :param us_pk PK del user story
        :param scrum_pk PK del scrum master
        """
    scrum= User.objects.get(pk=scrum_pk)
    user= User.objects.get(pk=user_pk)
    us= UserStory.objects.get(pk=us_pk)

    #Envia el correo al usuario asignado y al scrum master
    mail_subject = 'Notificación : Tiempo limite de User Story superado'
    message_scrum = render_to_string('us/email_scrum.html', {
        'user': scrum,
        'user_story': us,
        'encargado': user,
    })
    message_user = render_to_string('us/email_scrum.html', {
        'user': user,
        'user_story': us,
    })

    scrum.email_user(mail_subject, message_scrum)
    user.email_user(mail_subject, message_user)

    logger.info("Envio de mail a" + scrum.username + " y " + user.username +" al superar su tiempo el us" + us.titulo + " del proyecto " + us.proyecto.nombre)





@task(name='us_desasignado')
def us_desasignado_task(user_pk, us_pk):

    """Task Celery que envia un mail al usuario cuando se le desasigna un User Story
        :param userpk Pk del usuario al cual esta asignado el user story
        :param us_pk PK del user story

        """
    user= User.objects.get(pk=user_pk)
    us= UserStory.objects.get(pk=us_pk)

    #Envia el correo al usuario que se le desasigno el us
    mail_subject = 'Notificación : Fuiste desasignado de un Us'
    message_user = render_to_string('us/notif_desasignar.html', {
        'user': user,
        'user_story': us,
    })
    user.email_user(mail_subject, message_user)

    logger.info("Envio de mail a"+  user.username +" por desasignacion del" + us.titulo + " del proyecto " + us.proyecto.nombre)
