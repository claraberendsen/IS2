from auditlog.registry import auditlog
from django.db import models
from django.contrib.auth.models import Group
from Proyecto.models import Proyecto
from team.models import team
import random
import os
from Kanban.models import Flujo, Fase
# Create your models here.

def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext

def upload_file_path(instance, filename):
    new_filename = random.randint(1,3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "Archivos Adjuntos/{new_filename}/{final_filename}".format(
            new_filename=new_filename,
            final_filename=final_filename
            )
EstadosUS = (
    (1, 'Inicial'),
    (2, 'En Desarrollo'),
    (3, 'Terminado'),
    (4, 'Cancelado'),
)

class UserStory(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING)
    titulo = models.CharField(max_length=80)
    descripcion_corta = models.CharField(max_length=50)
    descripcion_larga = models.CharField(max_length=1000, null=True, blank=True)
    criterios_de_aceptacion = models.CharField(max_length=80, null=True, blank=True)
    urgencia = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    valor_de_negocio = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    valor_tecnico = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    prioridad = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    flujo = models.ForeignKey(Flujo, null=True, blank=True, on_delete=models.DO_NOTHING)
    tiempo_estimado_de_terminacion = models.DecimalField(default=0.00, max_digits=100, decimal_places=0)
    horas_trabajadas = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    estado = models.IntegerField(choices=EstadosUS)
    comentario_extra = models.CharField(max_length=80, null=True, blank=True)
    encargado = models.ForeignKey(team, on_delete=models.SET_NULL, null=True, blank=True)
    archivo = models.FileField(upload_to=upload_file_path, null=True, blank=True)
    estado_actual = models.CharField(max_length=20, null=True, blank=True, default='To do')
    fase_actual = models.ForeignKey(Fase, null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.titulo) + " -- estimado: " + str(self.tiempo_estimado_de_terminacion) + "hs."

    class Meta:
        default_permissions = []
        permissions = (
            ("listar_us", "Ver lista de User Stories"),
            ("modificar_us", "Modificar User Stories"),
            ("redefinir_us", "Redefinir User Stories"),
            ("crear_us", "Crear User Stories"),
            ("listar_us_history", "Ver historial de los User Stories"),
            ("eliminar_us", "Cancelar User Stories"),
            ("ver_us", "Ver User Stories")
        )

class UserStoryHistory(models.Model):
    id_us = models.IntegerField()
    proyecto = models.IntegerField()
    titulo = models.CharField(max_length=80)
    descripcion_corta = models.CharField(max_length=80)
    descripcion_larga = models.CharField(max_length=80, null=True, blank=True)
    criterios_de_aceptacion = models.CharField(max_length=80, null=True, blank=True)
    urgencia = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    valor_de_negocio = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    valor_tecnico = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    prioridad = models.DecimalField(default=0.00, max_digits=2, decimal_places=0)
    flujo = models.CharField(max_length=80, null=True, blank=True)
    tiempo_estimado_de_terminacion = models.DecimalField(default=0.00, max_digits=100, decimal_places=0)
    horas_trabajadas = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    estado = models.IntegerField(default=1)
    comentario_extra = models.CharField(max_length=80, null=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now_add=True)
    autor = models.CharField(max_length=80)
    archivo = models.FileField(upload_to=upload_file_path, null=True, blank=True)
    estado_actual = models.CharField(max_length=20, null=True, blank=True)
    fase_actual = models.IntegerField(default=1, null=True, blank=True)
    def __str__(self):
        return str(self.titulo)

    class Meta:
        default_permissions = []
        permissions = (

        )

auditlog.register(UserStory)