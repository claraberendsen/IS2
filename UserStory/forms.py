from django import forms

from .models import UserStory
from Accounts.models import User, Rol
from Proyecto.models import Proyecto
from Kanban.models import Flujo

class UserStoryForm(forms.ModelForm):
    titulo = forms.CharField(label='titulo', max_length=80,
                                  widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'titulo', }))
    descripcion_corta = forms.CharField(label='descripcion_corta', max_length=50,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'descripcion_corta', }))

    descripcion_larga = forms.CharField(label='descripcion_larga', max_length=100, required=False,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'descripcion_larga', }))
    criterios_de_aceptacion = forms.CharField(label='criterios_de_aceptacion', max_length=70, required=False,
                        widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'criterios_de_aceptacion', }))
    urgencia = forms.DecimalField(label='urgencia', min_value=0.00,
                                 widget=forms.NumberInput(attrs={'class': 'form-control', 'name':'urgencia'}))
    valor_de_negocio = forms.DecimalField(label='valor_de_negocio', min_value=0.00,
                                 widget=forms.NumberInput(attrs={'class': 'form-control', 'name':'valor_de_negocio'}))
    valor_tecnico = forms.DecimalField(label='valor_tecnico', min_value=0.00,
                                 widget=forms.NumberInput(attrs={'class': 'form-control', 'name':'valor_tecnico'}))
    horas_trabajadas = forms.DecimalField(label='horas_trabajadas', min_value=0.00, required=False,
                                 widget=forms.NumberInput(attrs={'class': 'form-control', 'name':'horas_trabajadas'}))
    estado = forms.IntegerField(label='estado', min_value=0, required=False,
                                       widget=forms.NumberInput(attrs={'class': 'form-control', 'name': 'estado'}))
    # prioridad = forms.DecimalField(label='prioridad', min_value=0,
    #                                    widget=forms.NumberInput(attrs={'class': 'form-control', 'name': 'prioridad'}))
    flujo = forms.ModelChoiceField(queryset=Flujo.objects.all(), widget=forms.Select())
    tiempo_estimado_de_terminacion = forms.DecimalField(label='tiempo_estimado_de_terminacion', min_value=0,
                    widget=forms.NumberInput(attrs={'class': 'form-control', 'name': 'tiempo_estimado_de_terminacion'}))
    comentario_extra = forms.CharField(label='comentario_extra', max_length=70, required=False,
                             widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'comentario_extra', }))

    archivo = forms.FileField(required=False)

    class Meta:
        model = UserStory
        fields = [
            'titulo',
            'descripcion_corta',
            'urgencia',
            'valor_de_negocio',
            'valor_tecnico',
            'flujo',
            'tiempo_estimado_de_terminacion',
            'comentario_extra',
            'archivo'
        ]

