from django.db import models
from django.urls import reverse


class Cliente(models.Model):
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    cedula = models.CharField(max_length=15, unique=True)
    direccion = models.CharField(max_length=200, null=True, blank=True)
    telefono = models.CharField(max_length=10, null=True, blank=True)
    email = models.EmailField(max_length=100,unique=True)

    def get_absolute_url(self):
        return reverse('Cliente:detail', kwargs={'pk':self.pk})

    def __str__(self):
        return self.nombre + ' ' + self.apellido

    class Meta:
        default_permissions = []
        permissions = (
            ("crear_cliente", "Crear clientes"),
            ("modificar_cliente", "Modificar clientes"),
            ("eliminar_cliente", "Eliminar clientes")
        )