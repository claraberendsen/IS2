from django.urls import reverse_lazy
from django.views import generic
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import PermissionRequiredMixin

from .models import Cliente
"""
    ClienteCreate hereda de de la vista generica CreateView y PermissionRequiredMixin
    Requiere que el usuario posea el  permiso crear_cliente para poder ser utilizada
"""
class ClienteCreate(PermissionRequiredMixin, CreateView):
    model = Cliente
    template_name = 'clientes/cliente_create.html'
    fields = ['nombre', 'apellido', 'cedula', 'direccion', 'telefono', 'email']
    permission_required = 'Cliente.crear_cliente'
    raise_exception = True
    success_url = reverse_lazy('Cliente:cliente-list')

"""
       ClienteUpdate hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el usuario posea el  permiso modificar_cliente para poder ser utilizada
"""
class ClienteUpdate(PermissionRequiredMixin, UpdateView):
    model = Cliente
    template_name = 'clientes/cliente_mod.html'
    fields = ['nombre', 'apellido','cedula', 'direccion', 'telefono', 'email']
    permission_required = 'Cliente.modificar_cliente'
    raise_exception = True
    success_url = reverse_lazy('Cliente:cliente-list')


class ListaClientes(ListView):
    model = Cliente
    context_object_name = 'my_clients'
    template_name = 'clientes/clientes_listar.html'

"""
       ClienteDelete hereda de de la vista generica DeleteView y PermissionRequiredMixin
       Requiere que el usuario posea el permiso eliminar_cliente para poder ser utilizada
"""
class ClienteDelete(PermissionRequiredMixin, DeleteView):
    model = Cliente
    template_name = 'clientes/clientes_listar.html'
    permission_required = 'Cliente.eliminar_cliente'
    raise_exception = True
    success_url = reverse_lazy('Cliente:cliente-list')
