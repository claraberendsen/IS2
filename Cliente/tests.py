from django.test import TestCase
from django.urls import reverse
from Accounts.models import User
from Proyecto.models import Proyecto
from .models import Cliente
from guardian.shortcuts import assign_perm
from django.contrib import auth

# Create your tests here.
class ClienteCreateViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email='abc@gmail.com')
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)
    def test_crear_cliente(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        assign_perm('Cliente.crear_cliente', usuario)

        response = self.client.post(reverse('Cliente:cliente-add'),data={'nombre':'Luhana','apellido':'Gonzalez',
                                                                         'direccion':'San Lorenzo','telefono': 1233,
                                                                         'email': 'abc@gmail.com'}, follow=True)

        self.assertContains(response, 'Luhana', msg_prefix="error en Cliente/test_crear_cliente")

    def test_crear_cliente_sin_permiso(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)

        response = self.client.post(reverse('Cliente:cliente-add'), data={'nombre': 'Luhana', 'apellido': 'Gonzalez',
                                                                          'direccion': 'San Lorenzo', 'telefono': 1233,
                                                                          'email': 'abc@gmail.com'}, follow=True)

        self.assertEqual(response.status_code, 403, msg="error en Cliente/test_crear_cliente_sin_permiso")

class ClienteUpdateViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email='abc@gmail.com')
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_modificar_cliente(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        assign_perm('Cliente.crear_cliente', usuario)
        assign_perm('Cliente.modificar_cliente', usuario)
        id= Cliente.objects.create(nombre='Luhana',apellido='Gonzalez', direccion='San Lo', telefono=123, email='abc@gmail.com').id

        response = self.client.post(reverse('Cliente:cliente_modificar', kwargs={'pk':id}), data={'nombre': 'Luana'}, follow=True)
        self.assertContains(response, 'Luana', msg_prefix="error en Cliente/test_modificar_cliente")

    def test_modificar_cliente_sin_permiso(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        id = Cliente.objects.create(nombre='Luhana', apellido='Gonzalez', direccion='San Lo', telefono=123,
                                    email='abc@gmail.com').id

        response = self.client.post(reverse('Cliente:cliente_modificar', kwargs={'pk': id}), data={'nombre': 'Luana'},
                                    follow=True)
        self.assertEqual(response.status_code, 403, msg="error en Cliente/test_modificar_cliente_sin_permiso")

class ClienteDeleteViewTests(TestCase):
    def login(self):
        usuario = User(username='matias', email='abc@gmail.com')
        usuario.set_password('123456')
        usuario.save()
        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

    def test_eliminar_cliente(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        assign_perm('Cliente.crear_cliente', usuario)
        assign_perm('Cliente.eliminar_cliente', usuario)

        id= Cliente.objects.create(nombre='Luhana',apellido='Gonzalez', direccion='San Lo', telefono=123, email='abc@gmail.com').id

        response = self.client.post(reverse('Cliente:cliente_eliminar', kwargs={'pk': id}), follow=True)
        self.assertNotContains(response, 'Luhana', msg_prefix="error en Cliente/test_eliminar_cliente")

    def test_eliminar_cliente_sin_permiso(self):
        self.login()
        usuario = auth.get_user(self.client)
        assign_perm('Proyecto.modificar_proyecto', usuario)
        assign_perm('Cliente.crear_cliente', usuario)
        id = Cliente.objects.create(nombre='Luhana', apellido='Gonzalez', direccion='San Lo', telefono=123,
                                    email='abc@gmail.com').id

        response = self.client.post(reverse('Cliente:cliente_eliminar', kwargs={'pk': id}), follow=True)
        self.assertEqual(response.status_code, 403, msg="error en Cliente/test_eliminar_cliente_sin_permiso")