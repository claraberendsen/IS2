from django.contrib import auth
from django.test import TestCase
from django.urls import reverse
from guardian.shortcuts import assign_perm

import UserStory
from Accounts.models import User, Rol

# Create your tests here.
from Kanban.forms import FaseFormSet
from UserStory.models import UserStory
from Sprint.models import Sprint
from Kanban.models import Fase, Flujo
from Proyecto.models import Proyecto
from team.models import team

def instantiate_formset(formset_class, data, instance=None, initial=None):
    prefix = formset_class().prefix
    formset_data = {}
    for i, form_data in enumerate(data):
        for name, value in form_data.items():
            if isinstance(value, list):
                for j, inner in enumerate(value):
                    formset_data['{}-{}-{}_{}'.format(prefix, i, name, j)] = inner
            else:
                formset_data['{}-{}-{}'.format(prefix, i, name)] = value
    formset_data['{}-TOTAL_FORMS'.format(prefix)] = len(data)
    formset_data['{}-INITIAL_FORMS'.format(prefix)] = 0

    if instance:
        return formset_class(formset_data, instance=instance, initial=initial)
    else:
        return formset_class(formset_data, initial=initial)



class FlujoCreate(TestCase):
        def login(self):
            usuario = User(username='matias', email="abc@gmail.com")
            usuario.set_password('123456')
            usuario.save()

            self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)


        def test_crear_flujo_sin_permiso(self):
            """
            Se ejecuta el setup inicial del test y luego se prueba crear un Daily perteneciente a un UserStory ya
            existente para esto se le brinda, al usuario los permisos adecuados(crear_daily y listar_daily) y
            mediante una llamada post con el id del proyecto, del UserStory creamos el objeto, nos aseguramos que
            la operacion fue exitosa con AssertContains
            """
            self.login()
            usuario = auth.get_user(self.client)

            assign_perm('Proyecto.modificar_proyecto', usuario)
            rol = Rol.objects.create(name='SCRUM', isProyectoRol=True)


            proyecto = Proyecto.objects.create(nombre='IS2', fechafin='2018-04-06',
                                               fechainicio='2018-04-06')
            team.objects.create(proyecto=proyecto, user=usuario, horas_trabajadas=2, rol=rol)
            response = self.client.post(reverse('Proyecto:Kanban:crearflujo',
                                                kwargs={'pk': proyecto.id}),
                                        data={'nombre': 'Flujo1', 'fase': 'f1', 'orden': 1},
                                        follow=True)
            self.assertEqual(response.status_code, 403, msg="Error en Flujo, no se tienen los permisos/test_crear_flujo")

class ImportarFlujoTest(TestCase):

    def setup_user(self):
        """
        Crea un usuario e inicia sesion,
        :return: usuario
        """
        usuario = User(username='matias')
        usuario.set_password('123456')
        usuario.email = 'email@gmail.com'
        usuario.save()

        self.client.post('/login/', data={'username': usuario.username, 'password': '123456'}, follow=True)

        return usuario

    def test_importar_flujo_sin_permiso(self):

        usuario = self.setup_user()

        rol1 = Rol.objects.create(isProyectoRol=True)
        assign_perm('Kanban.ver_flujos', rol1)
        proyecto2 = Proyecto.objects.create(fechainicio='2018-04-12', fechafin='2018-04-13')
        userStory = UserStory.objects.create(proyecto=proyecto2, estado=1)
        t1 = team.objects.create(user=usuario, rol=rol1, proyecto=proyecto2)
        sprint = Sprint.objects.create(fechaInicio='2018-12-12', fechaFinal='2018-12-13', proyecto=proyecto2, estado=1)
        flujo3 = Flujo.objects.create(proyecto=proyecto2, nombre='flujo2')

        response = self.client.post(reverse('Proyecto:Kanban:importarF',
                                            kwargs={'pk': str(flujo3.proyecto.id), 'pks': str(flujo3.id),
                                                    'pkp': str(proyecto2.id)}), follow=True)

        self.assertEqual(response.status_code, 403)

