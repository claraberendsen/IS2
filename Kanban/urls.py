
from django.urls import path
from Kanban import views

app_name = 'Kanban'
urlpatterns = [
    path('crear/', views.FlujoCreate.as_view(), name='crearflujo'),
    path('listar/',views.FlujoList.as_view(), name='listarflujo'),
    path('<int:pkk>/eliminar/', views.FlujoBorrar.as_view(), name='flujo_borrar'),
    path('visualizar/', views.VerKanban.as_view(), name='visualizar'),
    path('visualizar/update/', views.updateFlujo, name='update'),
    path('visualizar/updateHoras/', views.updateHoras, name='updateHoras'),
    path('<int:pkk>/modificar/', views.FlujoUpdate.as_view(), name='flujo_update'),
    path('importar', views.Importar_Flujo.as_view(), name='importarFlujo'),
    path('<int:pks>/seleccionar_importar/<int:pkp>', views.importarFlujoView, name='importarF'),
]