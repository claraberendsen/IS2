from django.db import models
from auditlog.registry import auditlog
from Proyecto.models import Proyecto

"""Modelo de Flujo
    Campos: Nombre y Foreign Key a Proyecto
    Al flujo pertenecen los permisos"""

class Flujo (models.Model):
    nombre = models.CharField('Nombre flujo', max_length=50)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.DO_NOTHING)

    #Clase donde se definen los permisos
    class Meta:
        unique_together = ('proyecto', 'nombre')
        default_permissions = []
        permissions = (
            ("ver_flujos", "Ver lista flujos"),
            ("modificar_flujos", "Modificar Flujos"),
            ("crear_flujo", "Crear Flujos"),
            ("eliminar_flujos", "Eliminar Flujos"),
            ("mover_kanban", "Mover todos los elementos del kanban"),
            ("importar_flujo", "Importar flujos")
        )

    def __str__(self):
        return self.nombre

    def get_fases_flujo(self):
        #for i in self.faseaflujo.filter(flujo=self).order_by('orden'):
         #   print(i.fase)
        return self.faseaflujo.filter(flujo=self).order_by('orden')

    def get_us_flujo(self):
        return self.userstory_set.all()

"""Modelo de las fases de un flujo
    Campos: Nombre, orden que indica la posición de la fase dentro del flujo y
    foreign key al flujo.
"""

class Fase(models.Model):
    nombre = models.CharField('nombre de la fase', max_length=50, name='fase')
    orden = models.IntegerField('orden')
    flujo = models.ForeignKey(Flujo, on_delete=models.CASCADE, related_name='faseaflujo')

    # Clase donde se definen los permisos
    class Meta:
        default_permissions = []

    def verificar_borrar_fase(self):
        if self.userstory_set.count()!=0:
            return False
        return True

"""Modelo de Estado
    Campos: Nombre y orden que indica la posición del estado dentro de la fase y
    foreign key al Sprint que pertenece.
"""

# class Estado(models.Model):
#     nombre = models.CharField('estado', max_length=50)
#     orden = models.IntegerField('orden')
#     sp_asoc= models.ForeignKey(Sprint, on_delete=models.PROTECT, related_name='sp_asoc')
#
#     # Clase donde se definen los permisos
#     class Meta:
#         default_permissions = []
#
auditlog.register(Fase)
auditlog.register(Flujo)