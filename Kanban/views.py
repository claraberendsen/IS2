from django.http import JsonResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from guardian import mixins
from django.db import transaction
from django.shortcuts import render, get_object_or_404

from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DeleteView, UpdateView, DetailView
from guardian.decorators import permission_required_or_403

from Daily.forms import DailyForm
from Kanban.forms import FlujoForm, FaseFormSet, FaseModFormSet
from Daily.models import Daily
from Kanban.models import Flujo, Fase
from Proyecto.models import Proyecto
from UserStory.models import UserStory, UserStoryHistory
from UserStory.tasks import mail_us_superado_task

"""View que maneja la visualización del tablero Kanban con los distintos
    flujos y user stories"""

class VerKanban(mixins.PermissionRequiredMixin, DetailView):
    model =Proyecto
    template_name = 'Kanban/kanban_ver.html'
    permission_required = 'Flujo.ver_flujos'
    raise_exception =True



    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pK
        :return: El nuevo context
        """
        # Llama a la implementación base para traer el contexto

        user = self.request.user
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])

        context = super().get_context_data(**kwargs)

        if user.has_perm('Kanban.mover_kanban', proyecto):
            context['userstories'] = UserStory.objects.filter(proyecto=self.kwargs['pk']).exclude(sprint=None)
        else:
            context['userstories'] = user.team_set.get(proyecto=self.kwargs['pk']).userstory_set.all()

        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        context['flujos']= Flujo.objects.filter(proyecto=proyecto).order_by('nombre')
        context['form_reunion'] = DailyForm
        return context


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])



"""View que maneja el listado de flujos"""
class FlujoList(mixins.PermissionRequiredMixin, ListView):
    model = Flujo
    template_name = 'Kanban/flujo_listar.html'
    context_object_name = 'mis_flujos'
    permission_required = 'Kanban.ver_flujos'
    raise_exception = True
    pk_url_kwarg = 'pkk'

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Llama a la implementación base para traer el contexto
        context = super().get_context_data(**kwargs)
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Kanban:listarflujo', kwargs={'pk': self.kwargs['pk']})

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])

    def get_queryset(self):
        return Flujo.objects.filter(proyecto=self.kwargs['pk'])


"""View que maneja el borrado de flujos"""
class FlujoBorrar(mixins.PermissionRequiredMixin, DeleteView):
    model = Flujo
    success_url = reverse_lazy('Proyecto:Kanban:listar_flujo')
    permission_required = 'Flujo.eliminar_flujos'
    raise_exception = True
    pk_url_kwarg = 'pkk'

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Kanban:listarflujo', kwargs={'pk': self.kwargs['pk']})

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])


"""View que maneja la creación de flujos"""
class FlujoCreate(mixins.PermissionRequiredMixin, CreateView):
    model = Flujo
    form_class = FlujoForm
    template_name = 'Kanban/flujo_crear.html'
    permission_required = 'Flujo.crear_flujo'
    raise_exception = True

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Kanban:listarflujo', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        """
               Es utilizada para generar los datos el contexto del view
                :return: contexto
                """
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['fases'] = FaseFormSet(self.request.POST)
        else:
            data['fases'] = FaseFormSet()
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        data['proyectoKey'] = self.kwargs['pk']
        data['proyecto'] = proyecto
        return data


    def form_valid(self, form):
        """
                Override de la función form_valid, para manejar el form normal y el inline_formset
                :return: form_valid del padre si es valido el form
                """
        context = self.get_context_data()
        fases = context['fases']
        #Se realiza una transacción atomica para asegurar consistencia de la bd
        with transaction.atomic():
            flujo = form.save(commit=False)
            flujo.proyecto=Proyecto.objects.get(pk=self.kwargs['pk'])
            flujo.save()
            self.object=form.save()
            if fases.is_valid():
                fases.instance = self.object
                fases.save()
        return super().form_valid(form)


    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])

"""View que maneja la modificación de flujos"""
class FlujoUpdate(UpdateView):
    model = Flujo
    form_class = FlujoForm
    template_name = 'Kanban/flujo_modificar.html'
    permission_required = 'Flujo.modificar_flujos'
    raise_exception = True
    pk_url_kwarg = 'pkk'

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Kanban:listarflujo', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        """
               Es utilizada para generar los datos el contexto del view, se utiliza la instancia del objeto
               para cargar los datos
                :return: contexto
                """
        data = super().get_context_data(**kwargs)
        if self.request.POST:
            data['fases'] = FaseModFormSet(self.request.POST, instance=self.object)

        else:
            data['fases'] = FaseModFormSet(instance=self.object)
        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        data['proyectoKey'] = self.kwargs['pk']
        data['proyecto'] = proyecto
        return data

    def form_valid(self, form):
        """
                       Override de la función form_valid, para manejar el form normal y el inline_formset
                       :return: form_valid del padre si es valido el form
                       """
        context = self.get_context_data()
        fases = context['fases']
        # Se realiza una transacción atomica para asegurar consistencia de la bd
        with transaction.atomic():
            flujo = form.save(commit=False)
            flujo.proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
            flujo.save()
            self.object = form.save()
            if fases.is_valid():
                fases.instance = self.object
                fases.save()
        return super().form_valid(form)

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        return get_object_or_404(Proyecto, pk=self.kwargs['pk'])



def updateFlujo(request, pk):
    """
    Esta vista actualiza la fase y estado al cual fue arrastrado el user story.
    :param request: get request http, con argumentos del estado nuevo, el estado anterior, fase nueva, fase anterior
    y el user story
    :param pk: primary key del proyecto actual
    :return: un Json response con datos de respuesta de verificacion
    """
    estado = request.GET.get('estado', None)
    estadoanterior = request.GET.get('estadoanterior', None)
    faseKey = request.GET.get('fase', None)
    faseKeyAnterior = request.GET.get('faseanterior', None)
    us = request.GET.get('us', None)
    user = request.user
    proyecto = Proyecto.objects.get(pk=pk)
    faseanterior = Fase.objects.get(pk=faseKeyAnterior)


    userStory = UserStory.objects.get(pk=us)

    fase = Fase.objects.get(pk=faseKey)

    if user.has_perm('Kanban.mover_kanban', proyecto):
        flag = True

    else:

        flag = False
        if (fase.orden + 1) == faseanterior.orden:
            flag = True
            if estado != 'Done' or estadoanterior != 'To do':
                flag = False
        elif fase.orden == (faseanterior.orden + 1):
            flag = True
            if estado != 'To do' or estadoanterior != 'Done':
                flag = False
        elif fase.orden == faseanterior.orden:
            flag = True

            if estado == 'Done' and estadoanterior != 'Doing':
                flag = False
            if estado == 'To do' and estadoanterior != 'Doing':
                flag = False

    if fase in userStory.flujo.faseaflujo.all():
        flag2 = True
        data = {
            'ok': True
        }
    else:
        flag2 = False
        data = {
            'ok': False
        }

    data['oksecuencial'] = flag

    if flag and flag2:
        Daily.objects.create(us=userStory, texto="De la fase: " + str(faseanterior.fase) + " estado: " + str(estadoanterior)
                                  + ". Paso a la fase: " + str(fase.fase) + " estado: " + str(estado), is_registro=True,
                                  autor=request.META.get('USERNAME'))
        userStory.estado_actual = estado
        userStory.fase_actual = fase

        us = userStory

        if us.fase_actual.orden == Fase.objects.filter(flujo=us.flujo).count() and us.estado_actual == 'Done':

        #     mail
            for team in proyecto.team_set.all():
                if team.user.has_perm('Kanban.mover_kanban', proyecto):
                    mail_subject = 'Fin de User Story'
                    message = render_to_string('Kanban/notif_scrum.html', {
                        'user': team.user,
                        'us': us
                    })
                    team.user.email_user(mail_subject, message)


        userStory.save()

    return JsonResponse(data)



class Importar_Flujo(mixins.PermissionRequiredMixin, ListView):
    model = Flujo
    template_name = 'Kanban/importar_flujo.html'
    permission_required = 'Kanban.importar_flujo'
    raise_exception = True

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return El objeto proyecto el cual se desea utlilizar:
        """
        pkp = self.kwargs['pk']
        return get_object_or_404(Proyecto, pk=pkp)

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return Una lista de objetos UserStory:
        """
        return Flujo.objects.all().exclude(proyecto_id=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)

        context['lista_importados']= Flujo.objects.all().exclude(proyecto_id=self.kwargs['pk'])

        proyecto = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = proyecto
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:Kanban:importarF', kwargs={'pk': self.kwargs['pk'],
                                                                 'pks': self.kwargs['pks'],
                                                                 'pkp': self.kwargs['pkp']})

@permission_required_or_403('Kanban.importar_flujo', (Proyecto, 'id', 'pk'))
def importarFlujoView(request, pk, pks, pkp):
    if request.method == 'POST':
        flujo = Flujo.objects.get(pk=pks)
        proyecto_actual = Proyecto.objects.get(pk=pkp)


        flujo2 = Flujo.objects.create(nombre=flujo.nombre, proyecto=proyecto_actual)


        fases = flujo.get_fases_flujo()
        for fase in flujo.get_fases_flujo():
            fase2 = fase
            fase2.pk = None
            fase2.save()
            flujo2.faseaflujo.add(fase2)

        flujo2.save()

        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context['pk'] = pkp
            return context

        # def get_success_url(self):
        #     """
        #     Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        #     :return: url de redireccionamiento
        #     """
        #     return reverse_lazy('Proyecto:sprint:impostarF', kwargs={'pk': self.kwargs['pk'],
        #                                                              'pks': self.kwargs['pks'],
        #                                                              'pkp': self.kwargs['pkp']})

        return HttpResponseRedirect(reverse_lazy('Proyecto:Kanban:listarflujo', kwargs = {'pk': pkp}))




def updateHoras(request, pk):
    """
    Esta vista actualiza las horas trabajadas de un user story
    :param request: el get request http con argumentos de horas y el primary key del user story
    :param pk: primary key del proyecto actual
    :return: un Json response
        """
    horas = request.GET.get('horas', None)
    us = request.GET.get('us', None)

    userStory = UserStory.objects.get(pk=us)
    fase = None
    if userStory.fase_actual != None:
        fase = userStory.fase_actual.id

    ush = UserStoryHistory.objects.create(id_us=userStory.id, proyecto=userStory.id, titulo=userStory.titulo,
                                    descripcion_corta=userStory.descripcion_corta,
                                    descripcion_larga=userStory.descripcion_larga,
                                    urgencia=userStory.urgencia, valor_tecnico=userStory.valor_tecnico,
                                    valor_de_negocio=userStory.valor_de_negocio, prioridad=userStory.prioridad,
                                    flujo=userStory.flujo.nombre,
                                    comentario_extra=userStory.comentario_extra, autor=request.user.username,
                                    horas_trabajadas=userStory.horas_trabajadas, estado=userStory.estado,
                                    archivo=userStory.archivo,
                                    fase_actual=fase, estado_actual=userStory.estado_actual)
    ush.save()
    userStory.horas_trabajadas += int(horas)
    userStory.save()
    ush2 = UserStoryHistory.objects.create(id_us=userStory.id, proyecto=userStory.id, titulo=userStory.titulo,
                                          descripcion_corta=userStory.descripcion_corta,
                                          descripcion_larga=userStory.descripcion_larga,
                                          urgencia=userStory.urgencia, valor_tecnico=userStory.valor_tecnico,
                                          valor_de_negocio=userStory.valor_de_negocio, prioridad=userStory.prioridad,
                                          flujo=userStory.flujo.nombre,
                                          comentario_extra=userStory.comentario_extra, autor=request.user.username,
                                          horas_trabajadas=userStory.horas_trabajadas, estado=userStory.estado,
                                          archivo=userStory.archivo,
                                          fase_actual=fase, estado_actual=userStory.estado_actual)
    ush2.save()
    proyecto= Proyecto.objects.get(pk=userStory.proyecto.pk)
    #se llama a la tarea que notifica si es que se supero las horas planificadas de trabajo

    if (userStory.horas_trabajadas > userStory.tiempo_estimado_de_terminacion):
        for team in proyecto.team_set.all():
         if(team.user.has_perm('Kanban.mover_kanban', proyecto)):
             t = 0
             mail_us_superado_task.delay(userStory.encargado.pk, userStory.pk, team.user.pk)

    d = Daily.objects.filter().last()
    d.texto = d.texto + ". Se utilizaron: " + str(horas) + " hs. de trabajo para el efecto"
    d.save()

    data = {
        'ok': True
    }


    return JsonResponse(data)
