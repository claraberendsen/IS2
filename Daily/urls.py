
from django.urls import path
from .views import CrearDaily, ModificarDaily, ListarDaily, EliminarDaily

# Urls del sistema
app_name = 'Daily'
urlpatterns = [
    path('', ListarDaily.as_view(), name="ListarDailies"),
    path('crear/', CrearDaily.as_view(), name="CrearDaily"),
    path('<int:pkd>/modificar/', ModificarDaily.as_view(), name="ModificarDaily"),
    path('<int:pkd>/eliminar/', EliminarDaily.as_view(), name="EliminarDaily")
]