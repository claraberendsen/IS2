from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.mixins import PermissionRequiredMixin
from guardian.mixins import PermissionRequiredMixin
from guardian import mixins
from django.views.generic.edit import CreateView, UpdateView
from Daily.forms import DailyForm
from Proyecto.models import Proyecto
from UserStory.models import UserStory
from .models import Daily
# Create your views here.


class CrearDaily(mixins.PermissionRequiredMixin, CreateView):
    """
    View que  gestiona la creacion de un  nuevo obejto de la clase Daily,
    herada de la clase mixins.PermissionRequiredMixin y CreateView,
    requiere del permiso daily_crear para poder ser utilizada
    """
    model = Daily
    form_class = DailyForm
    template_name = 'daily/daily_crear.html'
    permission_required = 'Daily.crear_daily'
    raise_exception = True

    def get_permission_object(self):
        """
         get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
         """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def form_valid(self, form):
        """
        form_valid permite alterar el form generico por defecto que acompanha a CreateView
        :param form, self: el form generico
        :return : el form generico modificado por form_valid
        """
        us = self.kwargs['pku']
        form.instance.us = UserStory.objects.filter(id=us).first()
        form.instance.autor = self.request.META.get('USERNAME')

        return super(CrearDaily, self).form_valid(form)

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:Daily:ListarDailies',
                            kwargs={'pk': self.kwargs['pk'], 'pku': self.kwargs['pku']})


class ListarDaily(mixins.PermissionRequiredMixin, generic.ListView):
    """
    ListarDaily hereda de de la vista generica ListView y PermissionRequiredMixin
    Requiere que el usuario posea el  permiso listar_daily para poder ser utilizada
    """
    model = Daily
    template_name = 'daily/daily_listar.html'
    permission_required = 'Daily.listar_daily'

    raise_exception = True

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['usKey']= self.kwargs['pku']
        context['proyecto'] = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Una lista de objetos Daily, que solo cuenta con un objeto
        """
        return Daily.objects.filter(us=self.kwargs['pku'])


class ModificarDaily(mixins.PermissionRequiredMixin, UpdateView):
    """
       ModificarDaily hereda de de la vista generica UpdateView y PermissionRequiredMixin
       Requiere que el usuario possea el  permiso modificar_daily para poder ser utilizada
    """
    model = Daily
    pk_url_kwarg = 'pkd'
    form_class = DailyForm
    template_name = 'daily/daily_modificar.html'
    permission_required = 'Daily.modificar_daily'
    raise_exception = True

    def get_permission_object(self):
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        proyecto = Proyecto.objects.get(pk=self.kwargs['pk'])
        # Add in the publisher
        context['proyectoKey'] = self.kwargs['pk']
        context['proyecto'] = get_object_or_404(Proyecto, pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        """
        Funcion que extrae los objetos necesarios desde la base de datos
        :return: Un objeto team
        """
        return Daily.objects.filter(id=self.kwargs['pkd'])

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:Daily:ListarDailies', kwargs={'pk': self.kwargs['pk'],
                                                                           'pku' : self.kwargs['pku']})


class EliminarDaily( mixins.PermissionRequiredMixin, generic.DeleteView):
    """
    EliminarDaily  hereda de de la vista generica DeleteView y PermissionRequiredMixin
    Requiere que el usuario posea el  permiso eliminar_daily para poder ser utilizada
    """
    model = Daily
    pk_url_kwarg = 'pkd'
    template_name = 'daily/daily_listar.html'
    permission_required = 'Daily.eliminar_daily'
    raise_exception = True

    def get_permission_object(self):
        """
        get_permission_object es una funcion necesaria para obtener los permisos que el usuario posee sobre el proyecto
        actual
        :return: El objeto proyecto el cual se desea utlilizar
        """
        pkp = self.kwargs['pk']
        return Proyecto.objects.get(pk=pkp)

    def get_context_data(self, **kwargs):
        """
        Funcion que permite cargar un parametro kwargs al contexto para que sea utilizado mas adelanta por el template
        :param kwargs: pk
        :return: El nuevo context
        """
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['proyecto'] = get_object_or_404(Proyecto, pk= self.kwargs['pk'])
        return context

    def get_success_url(self):
        """
        Es utilizada para obtener la direccion url a la cual se redirecciona en caso de exito de la opeacion
        :return: url de redireccionamiento
        """
        return reverse_lazy('Proyecto:UserStory:Daily:ListarDailies',
                            kwargs={'pk': self.kwargs['pk'], 'pku': self.kwargs['pku']})
