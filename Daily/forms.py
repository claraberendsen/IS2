from django.contrib.auth.forms import AuthenticationForm
from django import forms
from Daily.models import Daily
from Accounts.models import Rol, User


class DailyForm(forms.ModelForm):

    texto = forms.CharField(label='texto', max_length=200,
                               widget=forms.TextInput(attrs={'class':'form-control', 'name':'texto', }))
    autor = forms.CharField(label='texto', max_length=80, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'texto', }))

    class Meta:
        model = Daily
        fields = [
            'texto',

        ]
